﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheGame;
using TheGame.Abstract;
using TheGame.CommunicationServerModule;

namespace TheGameTests
{
    [TestClass]
    public class GMTests
    {
        private GM _gm;
        [TestInitialize]
        public void TestInitialize()
        {
            var playersInTeam = 3;
            _gm = new GM("127.0.0.1", 13, 8, 8, 2, 3, 2 * playersInTeam, 5000, 13, 0.2, 500, 0, 0, 0, 0, 0, 0, 0, false, true);
            for (var i = 1; i <= playersInTeam * 2; i++)
            {
                _gm.ReceiveRequest(new JoinToGameRequestMessage { agentId = i, teamId = i % 2 == 0 ? 0 : 1, wantToBeLeader = true }.ToJson(), out _, out _);
            }
            _gm.StartGame();
        }

        [TestMethod]
        public void When_GMReceivesCommunicationRequestWithNotRecognisedPlayer_Then_GMRespondsWithInvalidAction()
        {
            var request = new ExchangeInformationWithDataRequest
            {
                agentId = 1,
                data = "data from 1",
                withAgentId = 22
            }.ToJson();

            var response = _gm.ReceiveRequest(request, out _, out _);
            var responseModel = MessageModel.Decode(response);

            responseModel.Should().BeOfType<InvalidActionMessage>();
            responseModel.Should().Match(x => x.As<InvalidActionMessage>().agentId == 1);
        }

        [TestMethod]
        public void When_GMDoesNotAllowCommunicationBetweenEnemies_And_ReceivesCommunicationRequestWithEnemyPlayer_Then_GMRespondsWithInvalidAction()
        {
            _gm.CommunicationWithEnemiesAllowed = false;
            var request = new ExchangeInformationWithDataRequest
            {
                agentId = 1,
                data = "data from 1",
                withAgentId = 2
            }.ToJson();

            var response = _gm.ReceiveRequest(request, out _, out _);
            var responseModel = MessageModel.Decode(response);

            responseModel.Should().BeOfType<InvalidActionMessage>();
            ((InvalidActionMessage)responseModel).agentId.Should().Be(1);
        }

        [TestMethod]
        public void When_PlayerIsAskedForInformationByLeader_Then_GMRespondsWithInvalidActionUntilPlayerExchangesInfo()
        {
            var requestFromLeader = new ExchangeInformationWithDataRequest
            {
                agentId = 1,
                data = "data from leader",
                withAgentId = 3
            }.ToJson();
            var playerMakeMoveRequest = new MakeMoveRequestMessage
            {
                agentId = 3,
                moveDirection = Direction.Down
            }.ToJson();
            var playerRejection = new ExchangeInformationWithDataAgreement
            {
                agentId = 3,
                agreement = false,
                withAgentId = 1
            }.ToJson();
            const string dataFromPlayer = "data from player";
            var playerAcceptance = new ExchangeInformationWithDataAgreement
            {
                agentId = 3,
                agreement = true,
                withAgentId = 1,
                data = dataFromPlayer
            }.ToJson();

            _gm.ReceiveRequest(requestFromLeader, out _, out _);
            var response1 = MessageModel.Decode(_gm.ReceiveRequest(playerMakeMoveRequest, out _, out _));
            var response2 = MessageModel.Decode(_gm.ReceiveRequest(playerMakeMoveRequest, out _, out _));
            var response3 = MessageModel.Decode(_gm.ReceiveRequest(playerRejection, out _, out _));
            var response4 = MessageModel.Decode(_gm.ReceiveRequest(playerAcceptance, out _, out _));

            new[] { response1, response2, response3 }.All(x =>
              {
                  x.Should().BeOfType<InvalidActionMessage>();
                  ((InvalidActionMessage)x).agentId.Should().Be(3);
                  return true;
              });

            response4.Should().BeOfType<ExchangeInformationWithDataResponse>();
            ((ExchangeInformationWithDataResponse)response4).agentId.Should().Be(1);
            ((ExchangeInformationWithDataResponse)response4).agreement.Should().BeTrue();
            ((ExchangeInformationWithDataResponse)response4).data.Should().Be(dataFromPlayer);
        }

        [TestMethod]
        public void When_Player1RequestsExchangeInfo_And_Player3Accepts_Then_Player1GetsPlayer3Data()
        {
            var requestFrom1 = new ExchangeInformationWithDataRequest
            {
                agentId = 1,
                data = "data from 1",
                withAgentId = 3
            }.ToJson();

            const string dataFrom3 = "data from 3";
            var acceptanceFrom3 = new ExchangeInformationWithDataAgreement
            {
                agentId = 3,
                data = dataFrom3,
                withAgentId = 1,
                agreement = true
            }.ToJson();

            _gm.ReceiveRequest(requestFrom1, out _, out _);
            var response = MessageModel.Decode(_gm.ReceiveRequest(acceptanceFrom3, out _, out _));

            response.Should().BeOfType<ExchangeInformationWithDataResponse>();
            ((ExchangeInformationWithDataResponse)response).agentId.Should().Be(1);
            ((ExchangeInformationWithDataResponse)response).agreement.Should().BeTrue();
            ((ExchangeInformationWithDataResponse)response).data.Should().Be(dataFrom3);
        }


        //[TestMethod]
        //public void When_PlayerWithoutPieceWantsToDropPiece_Then_HeGetsInvalidAction()
        //{

        //    var gm = new GM(_server.Object, 12, 6, 2, 4, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);

        //    var result = gm.ReceiveRequest(new PutPieceRequestMessage()
        //    {
        //        agentId = 1
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}
        //[TestMethod]
        //public void When_PlayerWithoutPieceWantsToDestroyPiece_Then_HeGetsInvalidAction()
        //{
        //    var gm = new GM(_server.Object, 12, 6, 2, 4, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);

        //    var result = gm.ReceiveRequest(new DestroyPieceRequestMessage()
        //    {
        //        agentId = 1
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}
        //[TestMethod]
        //public void When_PlayerWantsToPickPieceWhereThereIsntAny_Then_HeGetsInvalidAction()
        //{
        //    var gm = new GM(_server.Object, 12, 6, 2, 4, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);

        //    var result = gm.ReceiveRequest(new PickPieceRequestMessage()
        //    {
        //        agentId = 1
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}

        //[TestMethod]
        //public void When_PlayerWithoutPieceWantsToCheckPiece_Then_HeGetsInvalidAction()
        //{
        //    var gm = new GM(_server.Object, 12, 6, 2, 4, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);

        //    var result = gm.ReceiveRequest(new CheckPieceRequestMessage()
        //    {
        //        agentId = 1
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}
        //[TestMethod]
        //public void When_PlayerWantsToLeaveGameArea_Then_HeGetsInvalidAction()
        //{
        //    var gm = new GM(_server.Object, 1, 6, 1, 4, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);

        //    var result = gm.ReceiveRequest(new MakeMoveRequestMessage()
        //    {
        //        agentId = 2,
        //        moveDirection = Direction.Down
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}
        //[TestMethod]
        //public void When_PlayerWantsToMoveToOccupiedField_Then_HeGetsInvalidAction()
        //{
        //    var gm = new GM(_server.Object, 1, 1, 1, 1, 2, 3, 10, 0.2, 123, 123, 123, 123, 123, 213, 123, 213);
        //    AddTwoPlayers(gm);
        //    System.Threading.Thread.Sleep(800);//wait for game to start

        //    gm.ReceiveRequest(new MakeMoveRequestMessage()
        //    {
        //        agentId = 1,
        //        moveDirection = Direction.Down
        //    }.ToJson());
        //    var result = gm.ReceiveRequest(new MakeMoveRequestMessage()
        //    {
        //        agentId = 2,
        //        moveDirection = Direction.Up
        //    }.ToJson());

        //    _server.Verify(x => x.ReceiveRequest(It.IsAny<string>(), It.IsAny<TCPConnection>()), Times.AtLeastOnce);
        //    result.Should().Match(x =>
        //        MessageModel.Decode(x).Type() == MessageType.InvalidAction
        //    );
        //}

        //[TestMethod]
        //public void When_PlayerWantsToMove_Then_HeGetsTimePenaltyAccordingToConfiguration()
        //{
        //    var baseTimePenalty = 1234;
        //    var moveTimePenaltyMultiplier = 2;
        //    var expectedTimePenalty = baseTimePenalty * moveTimePenaltyMultiplier;
        //    var gm = new GM(_server.Object, 1, 1, 1, 1, 2, 3, 10, 0.2, baseTimePenalty, moveTimePenaltyMultiplier, 1, 1, 1, 1, 1, 1);
        //    AddTwoPlayers(gm);
        //    //System.Threading.Thread.Sleep(800);//wait for game to start

        //    var result = gm.ReceiveRequest(new MakeMoveRequestMessage()
        //    {
        //        agentId = 1,
        //        moveDirection = Direction.Up
        //    }.ToJson());

        //    var resultModel = MessageModel.Decode(result) as MakeMoveResponseMessage;

        //    resultModel.Should().NotBeNull();
        //    var resultPenalty = resultModel.waitUntilTime - resultModel.timestamp;
        //    resultPenalty.Should().BeGreaterOrEqualTo(expectedTimePenalty);
        //}



        //private void AddTwoPlayers(GM gm)
        //{
        //    gm.ReceiveRequest(new JoinToGameRequestMessage()
        //    {
        //        agentId = 1,
        //        teamId = 0,
        //        wantToBeLeader = true
        //    }.ToJson());
        //    gm.ReceiveRequest(new JoinToGameRequestMessage()
        //    {
        //        agentId = 2,
        //        teamId = 1,
        //        wantToBeLeader = true
        //    }.ToJson());
        //}
    }
}
