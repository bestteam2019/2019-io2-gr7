﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheGame;
using TheGame.CommunicationServerModule;

namespace TheGameTests
{
    [TestClass]
    public class TCPConnectionTests
    {
        private CommunicationServer _communicationServer;
        [TestInitialize]
        public void Initialize()
        {
            _communicationServer = new CommunicationServer();
        }

        [TestMethod]
        public void When_CommunicationServerIsConfiguredAndStartsListening_Then_ItsPossibleToConnectToSpecifiedPort()
        {
            //arrange
            const string address = "127.0.0.1";
            const int port = 1444;
            _communicationServer.Configure(port);
            _communicationServer.StartListening();
            var client = new TcpClient();

            //act
            client.Connect(address,port);
            var connected = client.Connected;

            //assert
            connected.Should().BeTrue();
        }


        [TestMethod]
        public void When_MultipleMessagesInStream_Then_GetSingleMessageShouldReturnOneMessage()
        {
            //arrange
            const string address = "127.0.0.1";
            const int port = 1244;
            IPAddress ipAddress = Dns.Resolve("localhost").AddressList[0];
            var listener = new TcpListener(ipAddress,port);
            listener.Start();
            var client = new TcpClient();
            client.Connect(address, port);
            var server = listener.AcceptTcpClient();

            var message1 = new GameStartMessage().ToJson();
            var message2 = new JoinToGameRequestMessage().ToJson();


            //act
            client.SendMessage(message1, MessageType.GameStart);
            client.SendMessage(message2, MessageType.JoinToGameRequest);
            var result = server.GetSingleMessage();
            var result2 = server.GetSingleMessage();

            //assert
            result.Should().Be(message1);
            result2.Should().Be(message2);
        }
    }
}
