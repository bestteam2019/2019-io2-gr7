﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheGame;
using TheGame.CommunicationServerModule;
using FluentAssertions;

namespace TheGameTests
{
    [TestClass]
    public class CommunicationServerTests
    {
        private CommunicationServer _communicationServer;
        private TcpClient _playerClient;
        private TcpClient _player2Client;
        private TcpClient _gmClient;

        private const int _serverPort = 444;

        [TestInitialize]
        public void Initialize()
        {
            _communicationServer = new CommunicationServer();
            _communicationServer.Configure(_serverPort);
            _communicationServer.StartListening();
           
        }

        [TestMethod]
        public void When_InitializingPhase_And_AgentsWantToConnectBeforeGMConnects_Then_ServerReturnsGMNotConnectedYet()
        {
            //arrange
            _playerClient = CreateAndConnectClient();
            var joinGameRequest = new JoinToGameRequestMessage().ToJson();

            //act
            _playerClient.SendMessage(joinGameRequest, MessageType.JoinToGameRequest);
            var response = _playerClient.GetSingleMessage();

            //assert
            TypeOf(response).Should().Be(MessageType.GMNotConnectedYet);
        }

        [TestMethod]
        public void When_InitializingPhase_And_AgentSendsCheckPieceRequest_Then_ServerReturnsInvalidAction()
        {
            //arrange
            _playerClient = CreateAndConnectClient();
            var checkPieceRequest = new CheckPieceRequestMessage().ToJson();

            //act
            _playerClient.SendMessage(checkPieceRequest, MessageType.CheckPieceRequest);
            var response = _playerClient.GetSingleMessage();

            //assert
            TypeOf(response).Should().Be(MessageType.InvalidAction);
        }

        [TestMethod]
        public void GMCanConnectToServer()
        {
            //arrange
            _playerClient = CreateAndConnectClient();
            var gmJoinGameRequest = new GMJoinToGameRequestMessage().ToJson();

            //act
            _playerClient.SendMessage(gmJoinGameRequest, MessageType.GMJoinToGameRequest);
            var response = _playerClient.GetSingleMessage();

            //assert
            var decodedResponse = MessageModel.Decode(response, out _);
            decodedResponse.Should().BeOfType<GMJoinToGameResponseMessage>();
            ((GMJoinToGameResponseMessage)decodedResponse).isConnected.Should().BeTrue();
        }

        [TestMethod]
        public void When_ServerAlreadyHoldsGMConnection_Then_ServerRejectsGMJoinRequests()
        {
            //arrange
            _gmClient = CreateAndConnectClient();
            var gmClient2 = CreateAndConnectClient();
            var gmJoinGameRequest = new GMJoinToGameRequestMessage().ToJson();

            //act
            _gmClient.SendMessage(new GMJoinToGameRequestMessage().ToJson(), MessageType.GMJoinToGameRequest);
            gmClient2.SendMessage(new GMJoinToGameRequestMessage().ToJson(), MessageType.GMJoinToGameRequest);

            var response = gmClient2.GetSingleMessage();

            //assert
            var decodedMessage = MessageModel.Decode(response, out _);
            decodedMessage.Should().BeOfType<GMJoinToGameResponseMessage>();
            ((GMJoinToGameResponseMessage)decodedMessage).isConnected.Should().BeFalse();
        }

        [TestMethod]
        public void When_GameRunningPhase_And_AgentSendsMessageWithId129_Then_ServerReturnsInvalidAction()
        {
            //arrange
            StartGame();
            var gameStartMessage = _playerClient.GetSingleMessage();
            var requestWithId129 = new Discover3x3Response().ToJson();

            //act
            _playerClient.SendMessage(requestWithId129, MessageType.Discover3x3Response);
            var response = _playerClient.GetSingleMessage();

            //assert
            TypeOf(response).Should().Be(MessageType.InvalidAction);
        }

        [TestMethod]
        public void When_InitializingPhase_And_AgentSendsGameStartMessage_Then_ServerReturnsInvalidAction()
        {
            //arrange
            _playerClient = CreateAndConnectClient();
            _gmClient = CreateAndConnectClient();

            //act
            _playerClient.SendMessage(new GameStartMessage().ToJson(), MessageType.GameStart);
            var response = _playerClient.GetSingleMessage();

            //assert
            TypeOf(response).Should().Be(MessageType.InvalidAction);
        }

        [TestMethod]
        public void When_GMSendsGameStart_Then_AllAcceptedPlayersReceiveThatMessage()
        {
            //arrange
            //act
            StartGame();
            var msg1 = _playerClient.GetSingleMessage();
            var msg2 = _player2Client.GetSingleMessage();

            //assert
            TypeOf(msg1).Should().Be(MessageType.GameStart);
            TypeOf(msg2).Should().Be(MessageType.GameStart);
        }

        //[TestMethod]
        //public void When_AgentSendsRequestToGM_Then_ServerAppendsUniqueAgentIdToMessage()
        //{
        //    //arrange
        //    StartGame();
        //    var moveDownRequest = new MakeMoveRequestMessage
        //    {
        //        moveDirection = Direction.Down
        //    }.ToJson();

        //    var moveUpRequest = new MakeMoveRequestMessage
        //    {
        //        moveDirection = Direction.Up
        //    }.ToJson();

        //    var agentMovingDownId = 0;
        //    var agentMovingUpId = 0;

        //    _gmConnection.Setup(x => x.SendRequestToParticipant(It.IsAny<string>()))
        //        .Callback((string message) =>
        //        {
        //            var request = (MakeMoveRequestMessage)MessageModel.Decode(message, out _);
        //            switch (request.moveDirection)
        //            {
        //                case Direction.Down:
        //                    agentMovingDownId = request.agentId;
        //                    break;
        //                case Direction.Up:
        //                    agentMovingUpId = request.agentId;
        //                    break;
        //            }
        //        });

        //    //act
        //    _communicationServer.ReceiveRequest(moveUpRequest, _participantConnection.Object);
        //    _communicationServer.ReceiveRequest(moveDownRequest, _participantConnection2.Object);

        //    //assert

        //    _gmConnection.Verify(x => x.SendRequestToParticipant(It.Is<string>(message =>
        //        TypeOf(message) == MessageType.MakeMoveRequest)), Times.Exactly(2));

        //    agentMovingDownId.Should().NotBe(0);
        //    agentMovingUpId.Should().NotBe(0);
        //    agentMovingDownId.Should().NotBe(agentMovingUpId);
        //}

        //[TestMethod]
        //public void When_GameIsRunning_And_AgentSendsMoveRequest_Then_AgentReceivesResponseFromGM()
        //{
        //    //arrange
        //    StartGame();
        //    var requestToGM = new MakeMoveRequestMessage
        //    {
        //        moveDirection = Direction.Up
        //    }.ToJson();

        //    var responseFromGM = "";
        //    _gmConnection.Setup(x => x.SendRequestToParticipant(It.Is<string>(y => TypeOf(y) == MessageType.MakeMoveRequest)))
        //        .Returns((string message) =>
        //        {
        //            var decodedMessage = (MakeMoveRequestMessage)MessageModel.Decode(message, out _);
        //            responseFromGM = new MakeMoveResponseMessage
        //            {
        //                agentId = decodedMessage.agentId
        //            }.ToJson();
        //            return responseFromGM;
        //        });

        //    //act
        //    var response = _communicationServer.ReceiveRequest(requestToGM, _participantConnection.Object);

        //    //assert
        //    _gmConnection.Verify(x => x.SendRequestToParticipant(It.Is<string>(y => TypeOf(y) == MessageType.MakeMoveRequest)), Times.Once);
        //    response.Should().Be(responseFromGM);
        //}


        private void StartGame()
        {
            _gmClient = CreateAndConnectClient();
            _playerClient = CreateAndConnectClient();
            _player2Client = CreateAndConnectClient();


            _gmClient.SendMessage(new GMJoinToGameRequestMessage().ToJson(), MessageType.GMJoinToGameRequest);

            var agentsIds = new List<int>{1,2};

            _playerClient.SendMessage(new JoinToGameRequestMessage().ToJson(), MessageType.JoinToGameRequest);
            _player2Client.SendMessage(new JoinToGameRequestMessage().ToJson(), MessageType.JoinToGameRequest);

            foreach (var agentId in agentsIds)
            {
                _gmClient.SendMessage(new GameStartMessage { agentId = agentId }.ToJson(), MessageType.GameStart);
            }
        }

        private MessageType TypeOf(string response)
        {
            MessageModel.Decode(response, out var type);
            return type;
        }

        private TcpClient CreateAndConnectClient()
        {
            var client = new TcpClient();
            client.Connect("127.0.0.1", _serverPort);
            return client;
        }
    }
}
