﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheGame;
using TheGame.CommunicationServerModule;
using FluentAssertions;
using TheGame.Abstract;

namespace TheGameTests
{
    [TestClass]
    public class PlayerCommunicationTests
    {
        private Mock<ICommunicationServer> _server = new Mock<ICommunicationServer>();

        [TestInitialize]
        public void TestInitialize()
        {

        }



        //[TestMethod]
        //public void When_PlayerReceivesTimePenalty_HeDoesntMakeMoveUntilThen()
        //{
        //    //arrange
        //    _server.Setup(x =>
        //            x.ReceiveRequest(
        //                It.Is((string str) => MessageModel.Decode(str).Type() == MessageType.MakeMoveRequest),
        //                It.IsAny<ITCPConnection>()))
        //        .Returns(new MakeMoveResponseMessage
        //        {
        //            timestamp = 123,
        //            waitUntilTime = 1233333
        //        }.ToJson());

        //    //act
        //    var player = new Player(1,_server.Object);
        //    player.ReceiveRequest(new GameStartMessage
        //    {
        //        boardSizeX = 10,
        //        boardSizeY = 10,
        //        goalAreaHeight = 3,
        //        initialXPosition = 2,
        //        initialYPosition = 2
        //    }.ToJson());

        //    //Nie ma sensu testowac timeoutow

        //    //assert
        //    _server.Verify(x=>x.ReceiveRequest(
        //        It.Is((string str) => MessageModel.Decode(str).Type() == MessageType.MakeMoveRequest),
        //        It.IsAny<ITCPConnection>()),
        //        Times.Once);
        //    _server.VerifyNoOtherCalls();

        //}

        //[TestMethod]
        //public void PlayerSynchronizesHisTimestampWithGM()
        //{
        //    //arrange
        //   todo???

        //}

    }
}
