﻿namespace TheGame
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.serverConfigGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.launchCSButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addressCSTextBox = new System.Windows.Forms.TextBox();
            this.portCSTextBox = new System.Windows.Forms.TextBox();
            this.serverLogCheckBox = new System.Windows.Forms.CheckBox();
            this.gameMasterConfigGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.exchangeInfoTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.putTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.discoverTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.moveTextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.baseTimePenaltyTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.pickTextBox = new System.Windows.Forms.TextBox();
            this.destroyTextBox = new System.Windows.Forms.TextBox();
            this.checkTextBox = new System.Windows.Forms.TextBox();
            this.launchGMButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.addressGMTextBox = new System.Windows.Forms.TextBox();
            this.portGMTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.badProbabilityTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.maxPiecesTextBoard = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pieceSpawnIntervalTextBox = new System.Windows.Forms.TextBox();
            this.numberOfGoalsTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.goalAreaHeightTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.boardHeightTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.boardWidthTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.numberOfPlayersTextBox = new System.Windows.Forms.TextBox();
            this.playerConfigGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.launchPlayerButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.teamIDTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.addressPlayerTextBox = new System.Windows.Forms.TextBox();
            this.portPlayerTextBox = new System.Windows.Forms.TextBox();
            this.playerLogCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.serverConfigGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gameMasterConfigGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.playerConfigGroupBox.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.serverConfigGroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gameMasterConfigGroupBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.playerConfigGroupBox, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.66666F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 736);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // serverConfigGroupBox
            // 
            this.serverConfigGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.serverConfigGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serverConfigGroupBox.Location = new System.Drawing.Point(3, 3);
            this.serverConfigGroupBox.Name = "serverConfigGroupBox";
            this.serverConfigGroupBox.Size = new System.Drawing.Size(978, 106);
            this.serverConfigGroupBox.TabIndex = 0;
            this.serverConfigGroupBox.TabStop = false;
            this.serverConfigGroupBox.Text = "Configure and launch the Communications Server";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.70098F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.5649F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.73412F));
            this.tableLayoutPanel2.Controls.Add(this.launchCSButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.serverLogCheckBox, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(972, 87);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // launchCSButton
            // 
            this.launchCSButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.launchCSButton.Location = new System.Drawing.Point(3, 3);
            this.launchCSButton.Name = "launchCSButton";
            this.launchCSButton.Size = new System.Drawing.Size(211, 81);
            this.launchCSButton.TabIndex = 1;
            this.launchCSButton.Text = "Launch";
            this.launchCSButton.UseVisualStyleBackColor = true;
            this.launchCSButton.Click += new System.EventHandler(this.LaunchCSButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.addressCSTextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.portCSTextBox, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(666, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(303, 81);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 41);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // addressCSTextBox
            // 
            this.addressCSTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.addressCSTextBox.Location = new System.Drawing.Point(128, 10);
            this.addressCSTextBox.Name = "addressCSTextBox";
            this.addressCSTextBox.ReadOnly = true;
            this.addressCSTextBox.Size = new System.Drawing.Size(172, 20);
            this.addressCSTextBox.TabIndex = 2;
            // 
            // portCSTextBox
            // 
            this.portCSTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.portCSTextBox.Location = new System.Drawing.Point(128, 50);
            this.portCSTextBox.Name = "portCSTextBox";
            this.portCSTextBox.Size = new System.Drawing.Size(172, 20);
            this.portCSTextBox.TabIndex = 3;
            this.portCSTextBox.Text = "13";
            // 
            // serverLogCheckBox
            // 
            this.serverLogCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverLogCheckBox.AutoSize = true;
            this.serverLogCheckBox.Location = new System.Drawing.Point(233, 3);
            this.serverLogCheckBox.Name = "serverLogCheckBox";
            this.serverLogCheckBox.Size = new System.Drawing.Size(427, 81);
            this.serverLogCheckBox.TabIndex = 2;
            this.serverLogCheckBox.Text = "Log to file when game ends";
            this.serverLogCheckBox.UseVisualStyleBackColor = true;
            // 
            // gameMasterConfigGroupBox
            // 
            this.gameMasterConfigGroupBox.Controls.Add(this.tableLayoutPanel4);
            this.gameMasterConfigGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameMasterConfigGroupBox.Location = new System.Drawing.Point(3, 115);
            this.gameMasterConfigGroupBox.Name = "gameMasterConfigGroupBox";
            this.gameMasterConfigGroupBox.Size = new System.Drawing.Size(978, 492);
            this.gameMasterConfigGroupBox.TabIndex = 1;
            this.gameMasterConfigGroupBox.TabStop = false;
            this.gameMasterConfigGroupBox.Text = "Configure and launch the Game Master";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.groupBox3, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.launchGMButton, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.88828F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77.11172F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(972, 473);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel9);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(489, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(480, 359);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Timing parameters";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.exchangeInfoTextBox, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.label21, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label16, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.putTextBox, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label17, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label18, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.discoverTextBox, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.moveTextBox, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label20, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.baseTimePenaltyTextBox, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label23, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.pickTextBox, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.destroyTextBox, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.checkTextBox, 1, 4);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 8;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(474, 340);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // exchangeInfoTextBox
            // 
            this.exchangeInfoTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.exchangeInfoTextBox.Location = new System.Drawing.Point(240, 307);
            this.exchangeInfoTextBox.Name = "exchangeInfoTextBox";
            this.exchangeInfoTextBox.Size = new System.Drawing.Size(231, 20);
            this.exchangeInfoTextBox.TabIndex = 20;
            this.exchangeInfoTextBox.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Location = new System.Drawing.Point(3, 294);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(231, 46);
            this.label21.TabIndex = 19;
            this.label21.Text = "Exchange info";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(3, 252);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(231, 42);
            this.label16.TabIndex = 16;
            this.label16.Text = "Put";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // putTextBox
            // 
            this.putTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.putTextBox.Location = new System.Drawing.Point(240, 263);
            this.putTextBox.Name = "putTextBox";
            this.putTextBox.Size = new System.Drawing.Size(231, 20);
            this.putTextBox.TabIndex = 15;
            this.putTextBox.Text = "1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(3, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(231, 42);
            this.label17.TabIndex = 13;
            this.label17.Text = "Destroy";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(3, 168);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(231, 42);
            this.label18.TabIndex = 10;
            this.label18.Text = "Check";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // discoverTextBox
            // 
            this.discoverTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.discoverTextBox.Location = new System.Drawing.Point(240, 95);
            this.discoverTextBox.Name = "discoverTextBox";
            this.discoverTextBox.Size = new System.Drawing.Size(231, 20);
            this.discoverTextBox.TabIndex = 8;
            this.discoverTextBox.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Location = new System.Drawing.Point(3, 84);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(231, 42);
            this.label19.TabIndex = 7;
            this.label19.Text = "Discover";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // moveTextBox
            // 
            this.moveTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.moveTextBox.Location = new System.Drawing.Point(240, 53);
            this.moveTextBox.Name = "moveTextBox";
            this.moveTextBox.Size = new System.Drawing.Size(231, 20);
            this.moveTextBox.TabIndex = 6;
            this.moveTextBox.Text = "1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(3, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(231, 42);
            this.label20.TabIndex = 5;
            this.label20.Text = "Move";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(231, 42);
            this.label22.TabIndex = 1;
            this.label22.Text = "Base time penalty";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // baseTimePenaltyTextBox
            // 
            this.baseTimePenaltyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.baseTimePenaltyTextBox.Location = new System.Drawing.Point(240, 11);
            this.baseTimePenaltyTextBox.Name = "baseTimePenaltyTextBox";
            this.baseTimePenaltyTextBox.Size = new System.Drawing.Size(231, 20);
            this.baseTimePenaltyTextBox.TabIndex = 2;
            this.baseTimePenaltyTextBox.Text = "500";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Location = new System.Drawing.Point(3, 126);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(231, 42);
            this.label23.TabIndex = 17;
            this.label23.Text = "Pick";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pickTextBox
            // 
            this.pickTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pickTextBox.Location = new System.Drawing.Point(240, 137);
            this.pickTextBox.Name = "pickTextBox";
            this.pickTextBox.Size = new System.Drawing.Size(231, 20);
            this.pickTextBox.TabIndex = 18;
            this.pickTextBox.Text = "1";
            // 
            // destroyTextBox
            // 
            this.destroyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.destroyTextBox.Location = new System.Drawing.Point(240, 221);
            this.destroyTextBox.Name = "destroyTextBox";
            this.destroyTextBox.Size = new System.Drawing.Size(231, 20);
            this.destroyTextBox.TabIndex = 12;
            this.destroyTextBox.Text = "1";
            // 
            // checkTextBox
            // 
            this.checkTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkTextBox.Location = new System.Drawing.Point(240, 179);
            this.checkTextBox.Name = "checkTextBox";
            this.checkTextBox.Size = new System.Drawing.Size(231, 20);
            this.checkTextBox.TabIndex = 9;
            this.checkTextBox.Text = "1";
            // 
            // launchGMButton
            // 
            this.launchGMButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.launchGMButton.Location = new System.Drawing.Point(3, 3);
            this.launchGMButton.Name = "launchGMButton";
            this.launchGMButton.Size = new System.Drawing.Size(211, 102);
            this.launchGMButton.TabIndex = 2;
            this.launchGMButton.Text = "Launch";
            this.launchGMButton.UseVisualStyleBackColor = true;
            this.launchGMButton.Click += new System.EventHandler(this.LaunchGMButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(489, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(480, 102);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server parameters";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.addressGMTextBox, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.portGMTextBox, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(474, 83);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(290, 41);
            this.label3.TabIndex = 0;
            this.label3.Text = "IP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(290, 42);
            this.label4.TabIndex = 1;
            this.label4.Text = "Port";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // addressGMTextBox
            // 
            this.addressGMTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.addressGMTextBox.Location = new System.Drawing.Point(299, 10);
            this.addressGMTextBox.Name = "addressGMTextBox";
            this.addressGMTextBox.Size = new System.Drawing.Size(172, 20);
            this.addressGMTextBox.TabIndex = 2;
            this.addressGMTextBox.Text = "127.0.0.1";
            // 
            // portGMTextBox
            // 
            this.portGMTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.portGMTextBox.Location = new System.Drawing.Point(299, 52);
            this.portGMTextBox.Name = "portGMTextBox";
            this.portGMTextBox.Size = new System.Drawing.Size(172, 20);
            this.portGMTextBox.TabIndex = 3;
            this.portGMTextBox.Text = "13";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel6);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(480, 359);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Board parameters";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.label11, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.badProbabilityTextBox, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.label10, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.maxPiecesTextBoard, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.pieceSpawnIntervalTextBox, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.numberOfGoalsTextBox, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.goalAreaHeightTextBox, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.boardHeightTextBox, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.boardWidthTextBox, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label15, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.numberOfPlayersTextBox, 1, 4);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 8;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(474, 340);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 294);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(231, 46);
            this.label11.TabIndex = 16;
            this.label11.Text = "Bad piece probability";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // badProbabilityTextBox
            // 
            this.badProbabilityTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.badProbabilityTextBox.Location = new System.Drawing.Point(240, 307);
            this.badProbabilityTextBox.Name = "badProbabilityTextBox";
            this.badProbabilityTextBox.Size = new System.Drawing.Size(231, 20);
            this.badProbabilityTextBox.TabIndex = 15;
            this.badProbabilityTextBox.Text = "0,2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(231, 42);
            this.label10.TabIndex = 13;
            this.label10.Text = "Max pieces on board";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // maxPiecesTextBoard
            // 
            this.maxPiecesTextBoard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.maxPiecesTextBoard.Location = new System.Drawing.Point(240, 263);
            this.maxPiecesTextBoard.Name = "maxPiecesTextBoard";
            this.maxPiecesTextBoard.Size = new System.Drawing.Size(231, 20);
            this.maxPiecesTextBoard.TabIndex = 12;
            this.maxPiecesTextBoard.Text = "13";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(231, 42);
            this.label9.TabIndex = 10;
            this.label9.Text = "Piece spawn interval";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pieceSpawnIntervalTextBox
            // 
            this.pieceSpawnIntervalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pieceSpawnIntervalTextBox.Location = new System.Drawing.Point(240, 221);
            this.pieceSpawnIntervalTextBox.Name = "pieceSpawnIntervalTextBox";
            this.pieceSpawnIntervalTextBox.Size = new System.Drawing.Size(231, 20);
            this.pieceSpawnIntervalTextBox.TabIndex = 9;
            this.pieceSpawnIntervalTextBox.Text = "5000";
            // 
            // numberOfGoalsTextBox
            // 
            this.numberOfGoalsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numberOfGoalsTextBox.Location = new System.Drawing.Point(240, 137);
            this.numberOfGoalsTextBox.Name = "numberOfGoalsTextBox";
            this.numberOfGoalsTextBox.Size = new System.Drawing.Size(231, 20);
            this.numberOfGoalsTextBox.TabIndex = 8;
            this.numberOfGoalsTextBox.Text = "3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(231, 42);
            this.label8.TabIndex = 7;
            this.label8.Text = "Number of goals";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // goalAreaHeightTextBox
            // 
            this.goalAreaHeightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.goalAreaHeightTextBox.Location = new System.Drawing.Point(240, 95);
            this.goalAreaHeightTextBox.Name = "goalAreaHeightTextBox";
            this.goalAreaHeightTextBox.Size = new System.Drawing.Size(231, 20);
            this.goalAreaHeightTextBox.TabIndex = 6;
            this.goalAreaHeightTextBox.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(231, 42);
            this.label7.TabIndex = 5;
            this.label7.Text = "Goal area height";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // boardHeightTextBox
            // 
            this.boardHeightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.boardHeightTextBox.Location = new System.Drawing.Point(240, 53);
            this.boardHeightTextBox.Name = "boardHeightTextBox";
            this.boardHeightTextBox.Size = new System.Drawing.Size(231, 20);
            this.boardHeightTextBox.TabIndex = 4;
            this.boardHeightTextBox.Text = "8";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(231, 42);
            this.label6.TabIndex = 3;
            this.label6.Text = "Board height";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(231, 42);
            this.label5.TabIndex = 1;
            this.label5.Text = "Board width";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // boardWidthTextBox
            // 
            this.boardWidthTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.boardWidthTextBox.Location = new System.Drawing.Point(240, 11);
            this.boardWidthTextBox.Name = "boardWidthTextBox";
            this.boardWidthTextBox.Size = new System.Drawing.Size(231, 20);
            this.boardWidthTextBox.TabIndex = 2;
            this.boardWidthTextBox.Text = "8";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(3, 168);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(231, 42);
            this.label15.TabIndex = 17;
            this.label15.Text = "Number of players";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numberOfPlayersTextBox
            // 
            this.numberOfPlayersTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numberOfPlayersTextBox.Location = new System.Drawing.Point(240, 179);
            this.numberOfPlayersTextBox.Name = "numberOfPlayersTextBox";
            this.numberOfPlayersTextBox.Size = new System.Drawing.Size(231, 20);
            this.numberOfPlayersTextBox.TabIndex = 18;
            this.numberOfPlayersTextBox.Text = "4";
            // 
            // playerConfigGroupBox
            // 
            this.playerConfigGroupBox.Controls.Add(this.tableLayoutPanel7);
            this.playerConfigGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerConfigGroupBox.Location = new System.Drawing.Point(3, 613);
            this.playerConfigGroupBox.Name = "playerConfigGroupBox";
            this.playerConfigGroupBox.Size = new System.Drawing.Size(978, 120);
            this.playerConfigGroupBox.TabIndex = 2;
            this.playerConfigGroupBox.TabStop = false;
            this.playerConfigGroupBox.Text = "Configure and launch a Player";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.91113F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.27399F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.81488F));
            this.tableLayoutPanel7.Controls.Add(this.launchPlayerButton, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.playerLogCheckBox, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(972, 101);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // launchPlayerButton
            // 
            this.launchPlayerButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.launchPlayerButton.Location = new System.Drawing.Point(3, 3);
            this.launchPlayerButton.Name = "launchPlayerButton";
            this.launchPlayerButton.Size = new System.Drawing.Size(211, 95);
            this.launchPlayerButton.TabIndex = 1;
            this.launchPlayerButton.Text = "Launch";
            this.launchPlayerButton.UseVisualStyleBackColor = true;
            this.launchPlayerButton.Click += new System.EventHandler(this.LaunchPlayerButton_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel8.Controls.Add(this.label14, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.teamIDTextBox, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.addressPlayerTextBox, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.portPlayerTextBox, 1, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(674, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(295, 95);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 33);
            this.label14.TabIndex = 5;
            this.label14.Text = "Team ID";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // teamIDTextBox
            // 
            this.teamIDTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teamIDTextBox.Location = new System.Drawing.Point(120, 68);
            this.teamIDTextBox.Name = "teamIDTextBox";
            this.teamIDTextBox.Size = new System.Drawing.Size(172, 20);
            this.teamIDTextBox.TabIndex = 4;
            this.teamIDTextBox.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 31);
            this.label12.TabIndex = 0;
            this.label12.Text = "IP";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(3, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 31);
            this.label13.TabIndex = 1;
            this.label13.Text = "Port";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // addressPlayerTextBox
            // 
            this.addressPlayerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.addressPlayerTextBox.Location = new System.Drawing.Point(120, 5);
            this.addressPlayerTextBox.Name = "addressPlayerTextBox";
            this.addressPlayerTextBox.Size = new System.Drawing.Size(172, 20);
            this.addressPlayerTextBox.TabIndex = 2;
            this.addressPlayerTextBox.Text = "127.0.0.1";
            // 
            // portPlayerTextBox
            // 
            this.portPlayerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.portPlayerTextBox.Location = new System.Drawing.Point(120, 36);
            this.portPlayerTextBox.Name = "portPlayerTextBox";
            this.portPlayerTextBox.Size = new System.Drawing.Size(172, 20);
            this.portPlayerTextBox.TabIndex = 3;
            this.portPlayerTextBox.Text = "13";
            // 
            // playerLogCheckBox
            // 
            this.playerLogCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerLogCheckBox.AutoSize = true;
            this.playerLogCheckBox.Location = new System.Drawing.Point(254, 3);
            this.playerLogCheckBox.Name = "playerLogCheckBox";
            this.playerLogCheckBox.Size = new System.Drawing.Size(414, 95);
            this.playerLogCheckBox.TabIndex = 2;
            this.playerLogCheckBox.Text = "Log to file when game ends";
            this.playerLogCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 736);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "The Game";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.serverConfigGroupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.gameMasterConfigGroupBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.playerConfigGroupBox.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox serverConfigGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox addressCSTextBox;
        private System.Windows.Forms.TextBox portCSTextBox;
        private System.Windows.Forms.Button launchCSButton;
        private System.Windows.Forms.GroupBox gameMasterConfigGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button launchGMButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox addressGMTextBox;
        private System.Windows.Forms.TextBox portGMTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox numberOfGoalsTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox goalAreaHeightTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox boardHeightTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox boardWidthTextBox;
        private System.Windows.Forms.GroupBox playerConfigGroupBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox maxPiecesTextBoard;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox pieceSpawnIntervalTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox badProbabilityTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox teamIDTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox addressPlayerTextBox;
        private System.Windows.Forms.TextBox portPlayerTextBox;
        private System.Windows.Forms.Button launchPlayerButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox putTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox destroyTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox checkTextBox;
        private System.Windows.Forms.TextBox discoverTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox moveTextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox baseTimePenaltyTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox pickTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox numberOfPlayersTextBox;
        private System.Windows.Forms.TextBox exchangeInfoTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox playerLogCheckBox;
        private System.Windows.Forms.CheckBox serverLogCheckBox;
    }
}

