﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheGame
{
    /// <summary>
    /// Reprezentacja wiadomości w postaci konkretnych klas
    /// </summary>
    public abstract class MessageModel
    {
        public abstract MessageType msgId { get; }

        public MessageType Type()
        {
            return msgId;
        }

        /// <summary>
        /// Zamiana obiektu klasy wiadomości na odpowiadający jej json
        /// </summary>
        /// <returns></returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Deserializacja wiadomości <paramref name="json"/>
        /// </summary>
        /// <param name="json"></param>
        /// <returns> Obiekt klasy zdeserializowanej wiadomości </returns>
        public static MessageModel Decode(string json)
        {
            return Decode(json, out _);
        }

        /// <summary>
        /// Deserializacja wiadmości <paramref name="json"/> i zapis jej typu w <paramref name="type"/>
        /// </summary>
        /// <param name="json"></param>
        /// <param name="type"></param>
        /// <returns> Obiekt klasy zdeserializowanej wiadomości </returns>
        public static MessageModel Decode(string json, out MessageType type)
        {
            try
            {
                type = ((dynamic)JsonConvert.DeserializeObject(json)).msgId;
            }
            catch
            {
                throw new Exception("Message did not contain msgId");
            }
            try
            {
                return _deserializers[type](json) as MessageModel;
            }
            catch
            {
                throw new Exception("unable to parse message");
            }
        }

        private static readonly Dictionary<MessageType, Func<string, object>> _deserializers = new Dictionary<MessageType, Func<string, object>>
        {
            [MessageType.CantMoveInThisDirection] = JsonConvert.DeserializeObject<CantMoveMessage>,
            [MessageType.GMNotConnectedYet] = JsonConvert.DeserializeObject<GMNotConnectedYetMessage>,
            [MessageType.InvalidAction] = JsonConvert.DeserializeObject<InvalidActionMessage>,
            [MessageType.GameStart] = JsonConvert.DeserializeObject<GameStartMessage>,
            [MessageType.GameOver] = JsonConvert.DeserializeObject<GameOverMessage>,
            [MessageType.JoinToGameRequest] = JsonConvert.DeserializeObject<JoinToGameRequestMessage>,
            [MessageType.JoinToGameResponse] = JsonConvert.DeserializeObject<JoinToGameResponseMessage>,
            [MessageType.GMJoinToGameRequest] = JsonConvert.DeserializeObject<GMJoinToGameRequestMessage>,
            [MessageType.GMJoinToGameResponse] = JsonConvert.DeserializeObject<GMJoinToGameResponseMessage>,
            [MessageType.MakeMoveRequest] = JsonConvert.DeserializeObject<MakeMoveRequestMessage>,
            [MessageType.PickPieceRequest] = JsonConvert.DeserializeObject<PickPieceRequestMessage>,
            [MessageType.CheckPieceRequest] = JsonConvert.DeserializeObject<CheckPieceRequestMessage>,
            [MessageType.MakeMoveResponse] = JsonConvert.DeserializeObject<MakeMoveResponseMessage>,
            [MessageType.Discover3x3Response] = JsonConvert.DeserializeObject<Discover3x3Response>,
            [MessageType.PutPieceRequest] = JsonConvert.DeserializeObject<PutPieceRequestMessage>,
            [MessageType.DestroyPieceRequest] = JsonConvert.DeserializeObject<DestroyPieceRequestMessage>,
            [MessageType.RequestDuringTimePenaltyResponse] = JsonConvert.DeserializeObject<RequestDuringTimePenaltyMessage>,
            [MessageType.PickPieceResponse] = JsonConvert.DeserializeObject<PickPieceResponseMessage>,
            [MessageType.Discover3x3Request] = JsonConvert.DeserializeObject<Discover3x3RequestMessage>,
            [MessageType.CheckPieceResponse] = JsonConvert.DeserializeObject<CheckPieceResponseMessage>,
            [MessageType.PutPieceResponse] = JsonConvert.DeserializeObject<PutPieceResponse>,
            [MessageType.DestroyPieceResponse] = JsonConvert.DeserializeObject<DestroyPieceResponse>,
            [MessageType.ExchangeInformationWithDataResponse] = JsonConvert.DeserializeObject<ExchangeInformationWithDataResponse>,
            [MessageType.ExchangeInformationWithDataAgreement] = JsonConvert.DeserializeObject<ExchangeInformationWithDataAgreement>,
            [MessageType.ExchangeInformationRequest] = JsonConvert.DeserializeObject<ExchangeInformationRequest>,
            [MessageType.ExchangeInformationWithDataRequest] = JsonConvert.DeserializeObject<ExchangeInformationWithDataRequest>
        };
    }

    /// <summary>
    /// Wiadomość błędu oznaczająca nieprawidłowy json
    /// </summary>
    public class msg0 : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)0;
        public override int agentId { get; set; }
        public long timestamp;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca brak odpowiedzi od Mistrza Gry (GM)
    /// </summary>
    public class msg1 : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)1;
        public override int agentId { get; set; }
        public long timestamp;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca brak odpowiedzi od agenta
    /// </summary>
    public class msg2 : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)2;
        public override int agentId { get; set; }
        public long timestamp;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca WybranoCalyPrzedzialDlaPsa
    /// </summary>
    public class msg3 : MessageModel
    {
        public override MessageType msgId { get; } = (MessageType)3;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca próbę wykonania czynności przez agenta, który jest w trakcie kary czasowej za ostatnią akcję
    /// </summary>
    public class RequestDuringTimePenaltyMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)4;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca brak możliwości ruchu agenta w żądanym przez niego kierunku
    /// </summary>
    public class CantMoveMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)5;
        public override int agentId { get; set; }
        public long timestamp;
    }
    /// <summary>
    /// Wiadomość błędu wysyłana do agenta, gdy przy jego próbie połączenia nie jest jeszcze podłączony Mistrz Gry (GM) 
    /// </summary>
    public class GMNotConnectedYetMessage : MessageModel
    {
        public override MessageType msgId { get; } = (MessageType)6;
    }
    /// <summary>
    /// Wiadomość błędu oznaczająca nieprawidłowość akcji żądanej przez agenta np. wyjście poza mapę gry
    /// </summary>
    public class InvalidActionMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = MessageType.InvalidAction;
        public override int agentId { get; set; }
        public long timestamp;
    }
    /// <summary>
    /// Wiadomość startowa, którą wszyscy połączeni agenci otrzymują w momencie rozpoczęcia gry
    /// </summary>
    public class GameStartMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = MessageType.GameStart;
        public override int agentId { get; set; }
        public int[] agentIdsFromTeam;
        public int teamLeaderId;
        public long timestamp;
        public int boardSizeX;
        public int boardSizeY;
        public int goalAreaHeight;
        public int initialXPosition;
        public int initialYPosition;
        public int numberOfGoals;
        public int numberOfPlayers;
        public int pieceSpawnDelay;
        public int maxNumberOfPiecesOnBoard;
        public float probabilityOfBadPiece;
        public int baseTimePenalty;
        public int tpm_move;
        public int tpm_discoverPieces;
        public int tpm_pickPiece;
        public int tpm_checkPiece;
        public int tpm_destroyPiece;
        public int tpm_putPiece;
        public int tpm_infoExchange;
    }
    /// <summary>
    /// Wiadomość końcowa, którą wszyscy połączeni agenci otrzymują w momencie zakończenia gry
    /// </summary>
    public class GameOverMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)33;
        public override int agentId { get; set; }
        public long timestamp;
        public int winningTeam;
    }
    /// <summary>
    /// Wiadomość dostarczona Mistrzowi Gry (GM) przez serwer, gdy do gry zostaje podłączony nowy agent
    /// </summary>
    public class JoinToGameRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)48;
        public int teamId;
        public override int agentId { get; set; }
        public bool wantToBeLeader;
    }
    /// <summary>
    /// Informacja zwrotna do agenta po próbie dołączenia do gry
    /// </summary>
    public class JoinToGameResponseMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)49;
        public override int agentId { get; set; }
        public long timestamp;
        public bool isConnected;
    }
    /// <summary>
    /// Żądanie o dołączenie jako Mistrz Gry (GM)
    /// </summary>
    public class GMJoinToGameRequestMessage : MessageModel
    {
        public override MessageType msgId { get; } = (MessageType)50;
    }
    /// <summary>
    /// Wiadomość zwrotna do potencjalnego Mistrza Gry (GM) po próbie dołączenia do gry
    /// </summary>
    public class GMJoinToGameResponseMessage : MessageModel
    {
        public override MessageType msgId { get; } = (MessageType)51;
        public bool isConnected;
    }
    /// <summary>
    /// Wiadomość z prośbą agenta o ruch
    /// </summary>
    public class MakeMoveRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)64;
        public override int agentId { get; set; }
        public Direction moveDirection;
        // 0-gora , 1-prawo , 2-dol, 3-lewo
    }
    /// <summary>
    /// Żądanie wykonania skanu na obszarze 3x3 przez agenta
    /// </summary>
    public class Discover3x3RequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)65;
        public override int agentId { get; set; }
    }
    /// <summary>
    /// Żądanie podniesienia kawałka przez agenta
    /// </summary>
    public class PickPieceRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)66;
        public override int agentId { get; set; }
    }
    /// <summary>
    /// Żądanie podniesienia kawałka przez agenta
    /// </summary>
    public class CheckPieceRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)67;
        public override int agentId { get; set; }
    }
    /// <summary>
    /// Żądanie zniszczenia odłamka przez agenta
    /// </summary>
    public class DestroyPieceRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)68;
        public override int agentId { get; set; }
    }
    /// <summary>
    /// Żądanie odłożenia odłamka przez agenta
    /// </summary>
    public class PutPieceRequestMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = MessageType.PutPieceRequest;
        public override int agentId { get; set; }
    }
    /// <summary>
    /// Żądanie komunikacji z wymianą danych od agenta do innego agenta
    /// </summary>
    public class ExchangeInformationWithDataRequest : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)70;
        public override int agentId { get; set; }
        public int withAgentId;
        public string data;
    }
    /// <summary>
    /// Prośba o komunikację między dwoma agentami
    /// </summary>
    public class ExchangeInformationRequest : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)71;
        public override int agentId { get; set; }
        public int withAgentId; // who wants to exchange data with you
        public long timestamp;
    }
    /// <summary>
    /// Pytanie o zgodę na komunikację i wymianę danych, nie zawiera danych w przypadku braku zgody
    /// </summary>
    public class ExchangeInformationWithDataAgreement : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)72;
        public override int agentId { get; set; }
        public bool agreement;
        public int withAgentId;
        public string data;
    }
    /// <summary>
    /// Wiadomość zwrotna do agenta po próbie ruchu, zawiera odległość do najbliższego kawałka na mapie
    /// </summary>
    public class MakeMoveResponseMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)128;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
        public int closestPiece;
    }

    /// <summary>
    /// Wiadomość zwrotna po żądaniu skanu na obszarze 3x3
    /// </summary>
    public class Discover3x3Response : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)129;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
        public BlockInfo[] closestPieces;

        public class BlockInfo
        {
            public int x;
            public int y;
            public int dist;
        }
    }
    /// <summary>
    /// Wiadomość zwrotna po żądaniu podniesienia odłamka przez agenta
    /// </summary>
    public class PickPieceResponseMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)130;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
    }
    /// <summary>
    /// Wiadomość zwrotna po żądaniu sprawdzenia odłamka przez agenta
    /// </summary>
    public class CheckPieceResponseMessage : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)131;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
        public bool isCorrect;
    }
    /// <summary>
    /// Wiadomość zwrotna po żądaniu zniszczenia odłamka przez agenta
    /// </summary>
    public class DestroyPieceResponse : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)132;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
    }
    /// <summary>
    /// Wiadomość zwrotna po żądaniu odłożenia odłamka przez agenta
    /// </summary>
    public class PutPieceResponse : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)133;
        public override int agentId { get; set; }
        public long timestamp;
        public long waitUntilTime;
        public int result;
        // 0 - piece w task area
        // 1 - piece w goal area ; zrealizowano cel
        // 2 - piece w goal area ; niezrealizowano celu
        // 3 - piece w goal area ; piece byl falszywy
    }
    /// <summary>
    /// Wiadomość zwrotna po żądaniu komunikacji i wymianie danych między dwoma agentami, nie zawiera danych w przypadku braku zgody
    /// </summary>
    public class ExchangeInformationWithDataResponse : MessageModelWithAgentId
    {
        public override MessageType msgId { get; } = (MessageType)134;
        public override int agentId { get; set; }
        public int withAgentId;
        public bool agreement;
        public long timestamp;
        public long waitUntilTime;
        public string data;
    }

    public abstract class MessageModelWithAgentId : MessageModel
    {
        public abstract int agentId { get; set; }
    }
}
