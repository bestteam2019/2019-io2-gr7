﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheGame.CommunicationServerModule;

namespace TheGame
{
    /// <summary>
    /// Okno pozwalające na ustawienie parametrów serwera komunikacyjnego,
    /// Mistrza Gry oraz graczy, a następnie uruchomienie ich 
    /// </summary>
    public partial class MainMenuForm : Form
    {
        Dictionary<int, CommunicationServer> servers = new Dictionary<int, CommunicationServer>();

        public MainMenuForm()
        {
            InitializeComponent();
            addressCSTextBox.Text = Dns.GetHostName();
        }

        /// <summary>
        /// Uruchamia serwer komunikacyjny na odpowiednim porcie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LaunchCSButton_Click(object sender, EventArgs e)
        {
            if(!Int32.TryParse(portCSTextBox.Text, out int port))
            {
                MessageBox.Show("Invalid port number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var server = new CommunicationServer();
            server.Configure(port);
            server.StartListening();
            server.AddLogForm(new LogForm("Server", 4096, 1));
            server.Logging(serverLogCheckBox.Checked);
            servers.Add(port, server);
            MessageBox.Show("Server started", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Parsuje parametry Mistrza Gry i go uruchamia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="SocketException"/>
        private void LaunchGMButton_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(portGMTextBox.Text, out int port))
            {
                MessageBox.Show("Invalid port number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //if (!servers.ContainsKey(port))
            //{
            //    MessageBox.Show("No server on this port", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            if (!Int32.TryParse(boardHeightTextBox.Text, out int height) || height < 1) // without goal area
            {
                MessageBox.Show("Invalid height", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(boardWidthTextBox.Text, out int width) || width < 1)
            {
                MessageBox.Show("Invalid width", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(goalAreaHeightTextBox.Text, out int goalAreaHeight) || goalAreaHeight < 1 || goalAreaHeight + 1 > height / 2)
            {
                MessageBox.Show("Invalid goal area height", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(numberOfGoalsTextBox.Text, out int goals) || goals < 1 || goals > goalAreaHeight * width)
            {
                MessageBox.Show("Invalid number of goals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(numberOfPlayersTextBox.Text, out int players) || players < 1 || players > goalAreaHeight * width * 2 || players % 2 != 0)
            {
                MessageBox.Show("Invalid number of players", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(pieceSpawnIntervalTextBox.Text, out int pieceInterval) || pieceInterval < 1)
            {
                MessageBox.Show("Invalid piece spawn interval", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(maxPiecesTextBoard.Text, out int maxPieces) || maxPieces < 1 || maxPieces > height * width)
            {
                MessageBox.Show("Invalid max number of pieces on board", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Double.TryParse(badProbabilityTextBox.Text, out double badLuck) || badLuck >= 1.0 || badLuck < 0.0)
            {
                MessageBox.Show("Invalid bad piece probability", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(baseTimePenaltyTextBox.Text, out int baseTime) || baseTime < 1)
            {
                MessageBox.Show("Invalid base time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(moveTextBox.Text, out int move) || move < 1)
            {
                MessageBox.Show("Invalid move time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(discoverTextBox.Text, out int discover) || discover < 1)
            {
                MessageBox.Show("Invalid discover time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(pickTextBox.Text, out int pick) || pick < 1)
            {
                MessageBox.Show("Invalid pick time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(checkTextBox.Text, out int check) || check < 1)
            {
                MessageBox.Show("Invalid check time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(destroyTextBox.Text, out int destroy) || destroy < 1)
            {
                MessageBox.Show("Invalid destroy time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(putTextBox.Text, out int put) || put < 1)
            {
                MessageBox.Show("Invalid put time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(exchangeInfoTextBox.Text, out int exchange) || exchange < 1)
            {
                MessageBox.Show("Invalid exchange info time penalty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                GM gm = new GM(addressGMTextBox.Text, port, height, width, goalAreaHeight, goals, players, pieceInterval, maxPieces, badLuck,
                    baseTime, move, discover, pick, check, destroy, put, exchange, true);
                MessageBox.Show("Game master started", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SocketException se)
            {
                if (se.SocketErrorCode != SocketError.TryAgain) throw;
            }
        }
        
        /// <summary>
        /// Uruchamia gracza z odpowiedniej drużyny, który próbuje się połączyć
        /// z serwerem komunikacyjnym na podanym adresie i porcie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="SocketException"/>
        private void LaunchPlayerButton_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(portPlayerTextBox.Text, out int port))
            {
                MessageBox.Show("Invalid port number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!Int32.TryParse(teamIDTextBox.Text, out int team) || team < 0 || team > 1)
            {
                MessageBox.Show("Invalid team number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //if (!servers.ContainsKey(port))
            //{
            //    MessageBox.Show("No server on this port", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            try
            {
                Player p = new Player(team, addressPlayerTextBox.Text, port);
                p.Logging(playerLogCheckBox.Checked);
                MessageBox.Show("Player started", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SocketException se)
            {
                if (se.SocketErrorCode != SocketError.TryAgain) throw;
            }
        }
    }
}
