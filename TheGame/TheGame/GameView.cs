﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    /// <summary>
    /// Okno podglądu rozgrywki
    /// </summary>
    public partial class GameView : Form
    {
        private int gah;
        private int h;
        private int w;
        private int maxP;
        private double badP;
        private int playersPerTeam;
        private string statText;

        private Label[,] fieldDescriptions;
        
        private int refreshCap;
        private Timer refreshTimer;
        public int refreshCounter;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gah">Wysokość strefy celu (goal area)</param>
        /// <param name="h">Wysokość mapy gry</param>
        /// <param name="w">Szerokość mapy gry</param>
        /// <param name="maxP">Maksymalna liczba kawałków na mapie gry</param>
        /// <param name="badP">Szansa na wadliwy (fałszywy) kawałek z przedziału [0, 1]</param>
        /// <param name="players">Liczba graczy uczestniczących w grze</param>
        /// <param name="refreshCap">Maksymalna liczba odświeżeń widoku mapy na sekundę</param>
        public GameView(int gah, int h, int w, int maxP, double badP, int players, int refreshCap = 60)
        {
            InitializeComponent();
            
            this.gah = gah;
            this.h = h;
            this.w = w;
            this.maxP = maxP;
            this.badP = badP;
            this.playersPerTeam = players / 2;

            this.refreshCap = refreshCap;
            refreshTimer = new Timer();
            refreshTimer.Interval = 1000 / refreshCap;
            refreshTimer.Tick += new EventHandler(RefreshTimer_Tick);
            refreshCounter = 0;
            refreshTimer.Start();

            fieldDescriptions = new Label[w, 2 * gah + h];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < 2 * gah + h; y++)
                {
                    Label l = new Label();
                    l.Dock = DockStyle.Fill;
                    l.TextAlign = ContentAlignment.MiddleCenter;
                    //l.ForeColor = Color.Black;
                    //l.Text = "test";

                    fieldDescriptions[x, y] = l;
                }
            }

            mainTableLayoutPanel.RowStyles[0].SizeType = SizeType.Percent;
            mainTableLayoutPanel.RowStyles[0].Height = 100 * gah / (2 * gah + h);
            mainTableLayoutPanel.RowStyles[1].SizeType = SizeType.Percent;
            mainTableLayoutPanel.RowStyles[1].Height = 100 * h / (2 * gah + h);
            mainTableLayoutPanel.RowStyles[2].SizeType = SizeType.Percent;
            mainTableLayoutPanel.RowStyles[2].Height = 100 * gah / (2 * gah + h);
            
            this.Text += $" (maxPieces={maxP}, badProb={badP})";
            statText = this.Text;
            this.Text += $" - Waiting for ({playersPerTeam}, {playersPerTeam}) players";

            DBTableLayoutPanel teamZeroTable = new DBTableLayoutPanel();
            teamZeroTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            teamZeroTable.Dock = DockStyle.Fill;
            teamZeroTable.ColumnCount = w;
            teamZeroTable.RowCount = gah;
            teamZeroGroupBox.Controls.Add(teamZeroTable);
            for (int i = 0; i < w; i++)
                teamZeroTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            for (int i = 0; i < gah; i++)
                teamZeroTable.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < gah; y++)
                    teamZeroTable.Controls.Add(fieldDescriptions[x, y], x, y);
            }

            DBTableLayoutPanel centerTable = new DBTableLayoutPanel();
            centerTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            centerTable.Dock = DockStyle.Fill;
            centerTable.ColumnCount = w;
            centerTable.RowCount = h;
            centerGroupBox.Controls.Add(centerTable);
            for (int i = 0; i < w; i++)
                centerTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            for (int i = 0; i < h; i++)
                centerTable.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                    centerTable.Controls.Add(fieldDescriptions[x, y + gah], x, y);
            }

            DBTableLayoutPanel teamOneTable = new DBTableLayoutPanel();
            teamOneTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            teamOneTable.Dock = DockStyle.Fill;
            teamOneTable.ColumnCount = w;
            teamOneTable.RowCount = gah;
            teamOneGroupBox.Controls.Add(teamOneTable);
            for (int i = 0; i < w; i++)
                teamOneTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            for (int i = 0; i < gah; i++)
                teamOneTable.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < gah; y++)
                    teamOneTable.Controls.Add(fieldDescriptions[x, y + gah + h], x, y);
            }
        }

        /// <summary>
        /// Sygnalizuje upłynięcie wystarczającej ilości czasu od ostatniego odświeżenia mapy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            refreshCounter++;
            if (refreshCounter == refreshCap)
                refreshCounter = 0;
        }

        /// <summary>
        /// Uaktualnia tytuł okna podglądu rozgrywki
        /// </summary>
        /// <param name="players0">Aktualna liczba graczy w drużynie 0 (niebieskiej)</param>
        /// <param name="players1">Aktualna liczba graczy w drużynie 1 (czerwonej)</param>
        public void UpdateTitle(int players0, int players1)
        {
            BackgroundWorker LocalTitleUpdater = new BackgroundWorker();
            LocalTitleUpdater.DoWork += DoUpdateTitle;
            LocalTitleUpdater.RunWorkerAsync((players0, players1));
        }

        /// <summary>
        /// Wykonuje aktualizację tytułu okna podglądu rozgrywki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoUpdateTitle(object sender, DoWorkEventArgs e)
        {

            (int players0, int players1)? arg = (e.Argument) as (int, int)?;
            if (arg.HasValue)
            {

                int players0 = arg.Value.players0;
                int players1 = arg.Value.players1;
                Invoke((MethodInvoker)delegate
                {
                    this.Text = statText;
                    if (players0 == playersPerTeam && players1 == playersPerTeam)
                        this.Text += $" - Game in progress";
                    else
                        this.Text += $" - Waiting for ({playersPerTeam - players0}, {playersPerTeam - players1}) players";
                });
            }
        }

        /// <summary>
        /// Wyświetla okno informujące o wygranej
        /// </summary>
        /// <param name="winningTeam">Drużyna która wygrała grę (0 lub 1)</param>
        public void WinEnd(int winningTeam)
        {
            Invoke((MethodInvoker)delegate
            {
                if (winningTeam == 0)
                    MessageBox.Show("Team 0 (blue) won!", "The game has ended", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Team 1 (red) won!", "The game has ended", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            });
        }

        /// <summary>
        /// Odświeża podgląd rozgrywki
        /// </summary>
        /// <param name="map">Mapa gry</param>
        public void UpdateMapView(GM.Field[,] map)
        {
            Invoke((MethodInvoker)delegate
            {
                for (int x = 0; x < w; x++)
                {
                    for (int y = 0, revY = 2 * gah + h - 1; y < 2 * gah + h; y++, revY--)
                    {
                        if (map[x, revY].player != null)
                        {
                            if (map[x, revY].player.team == 0)
                            {
                                fieldDescriptions[x, y].Text = "B" + map[x, revY].player.id;
                                fieldDescriptions[x, y].ForeColor = Color.Blue;
                            }
                            else
                            {
                                fieldDescriptions[x, y].Text = "R" + map[x, revY].player.id;
                                fieldDescriptions[x, y].ForeColor = Color.Red;
                            }

                            if (map[x, revY].player.carriedPiece != null)
                            {
                                if (map[x, revY].player.carriedPiece.real)
                                    fieldDescriptions[x, y].Text += "P";
                                else
                                    fieldDescriptions[x, y].Text += "F";
                            }
                        }
                        else
                        {
                            if (map[x, revY].piece != null)
                            {
                                if (map[x, revY].piece.real)
                                    fieldDescriptions[x, y].Text = "P";
                                else
                                    fieldDescriptions[x, y].Text = "F";
                            }
                            else if (map[x, revY].isGoal && map[x, revY].isAchieved)
                                fieldDescriptions[x, y].Text = "YG";
                            else if (!map[x, revY].isGoal && map[x, revY].isAchieved)
                                fieldDescriptions[x, y].Text = "N";
                            else if (map[x, revY].isGoal && !map[x, revY].isAchieved)
                                fieldDescriptions[x, y].Text = "G";
                            else
                                fieldDescriptions[x, y].Text = " ";

                            fieldDescriptions[x, y].ForeColor = Color.Black;
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Zmodyfikowana WinFormsowa tabela lepiej działająca przy zmianie rozmiarów okna,
        /// wykorzystana do zawarcia opisów poszczególnych pól mapy,
        /// </summary>
        private class DBTableLayoutPanel : TableLayoutPanel
        {
            public DBTableLayoutPanel()
            {
                DoubleBuffered = true;
            }
        }
    }
}
