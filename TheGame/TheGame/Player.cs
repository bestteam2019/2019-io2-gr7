﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using TheGame.Abstract;
using TheGame.CommunicationServerModule;

namespace TheGame
{
    /// <summary>
    /// Reprezentacja agenta jako obiektu klasy
    /// </summary>
    public class Player
    {
        private Timer _timer;
        private long _nextMoveTime;

        private int id;
        private int team; 
        private int goalAreaHeight;
        private bool isLeader;
        private int leaderId;
        private bool isGameRunning;
        private (int x, int y) position;
        private Piece carriedPiece;
        private Field[,] map;
        private Random r = new Random();
        private TcpClient connection;
        private HashSet<(int x, int y)> possibleGoalFieldsCoordinates = new HashSet<(int, int)>();
        private bool new_info = false;
        private bool expecting_data = false;
        private long basepenalty;
        private int movemult = 1;
        private int discmult = 1;
        private int chckmult = 1;
        private int commmult = 1;
        private int dstrmult = 1;
        private int putmult = 1;
        private double misfortune = 0.2;
        int role = 0;
        int[] ids = null;
        private int prefferedColumn;
        private int prefferedRow;
        private bool block = false;

        private List<(long time, Direction d)> directions = new List<(long time, Direction d)>();

        private bool logWhenGameIsOver = false;

        private LogForm myLogForm;

        /// <summary>
        /// Łączy nowego gracza z serwerem o adresie IP <paramref name="address"/> na porcie <paramref name="port"/>
        /// </summary>
        /// <param name="preferredTeam"></param>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public Player(int preferredTeam, string address, int port)
        {
            if (!Connect(address, port)) throw new SocketException(11002);
            _timer = new Timer();
            _timer.Start();
            team = preferredTeam;
            var request = new JoinToGameRequestMessage { teamId = team }.ToJson();
            var response = SendMessageAndWaitForResponse(request, MessageType.JoinToGameRequest);
            var decodedResponse = MessageModel.Decode(response) as JoinToGameResponseMessage;
            if (decodedResponse == null) throw new JsonException("Unexpected response from server");
            if (!decodedResponse.isConnected) throw new ApplicationException("Unable to join a game on server");

            isGameRunning = true;

            myLogForm = new LogForm("Player - team " + preferredTeam);
            myLogForm.Show();
            myLogForm.Log("Waiting for game to start!");
            Task.Run(() => WaitForGameStart());
        }

        /// <summary>
        /// Strategia agenta
        /// </summary>
        void Run()
        {
            try
            {
                while (isGameRunning)
                {
                    _timer.SleepUntilTime(_nextMoveTime);
                    lock (this)
                    {
                        if (isGameRunning)
                        {
                            if (block && role == -2)
                            {
                                //horizontal positioning
                                if (position.x > prefferedColumn)
                                {
                                    switch (Move(Direction.Left))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                else if (position.x < prefferedColumn)
                                {
                                    switch (Move(Direction.Right))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                //vertical positioning
                                else if (position.y < prefferedRow)
                                {
                                    switch (Move(Direction.Up))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                else if (position.y > prefferedRow)
                                {
                                    switch (Move(Direction.Down))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                else
                                {
                                    switch (Discover())
                                    {

                                    }
                                }
                            }
                            else if (carriedPiece != null)
                            {
                                if (!carriedPiece.real.HasValue)
                                {
                                    if ((chckmult + dstrmult) < (movemult * 16 + putmult) * misfortune)
                                        CheckPiece();
                                    else
                                        carriedPiece.real = true;
                                }
                                else if (carriedPiece.real ?? false)
                                {
                                    //move to goal area
                                    if (team == 0 && position.y >= goalAreaHeight)
                                    {
                                        switch (Move(Direction.Down))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                    else if (team == 1 && position.y < map.GetLength(1) - goalAreaHeight)
                                    {
                                        switch (Move(Direction.Up))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }

                                    //find field to put a piece
                                    else if (!map[position.x, position.y].isGoal.HasValue)
                                    {
                                        if (!isLeader && new_info)
                                        {
                                            new_info = false;
                                            if (commmult < movemult * (ids.Length - 1) * 2)
                                                Communicate();
                                        }
                                        else
                                        {
                                            Put();
                                            new_info = true;
                                        }
                                    }
                                    else
                                    {
                                        (int x, int y) nearest = FindNearestPossibleGoal(position);
                                        if (nearest.x != position.x)
                                        {
                                            if (nearest.y != position.y)
                                            {
                                                Direction d1 = position.x < nearest.x ? Direction.Right : Direction.Left;
                                                Direction d2 = position.y < nearest.y ? Direction.Up : Direction.Down;
                                                switch (Move(d1))
                                                {
                                                    case Status.InvalidMove:
                                                        switch (Move(d2))
                                                        {
                                                            case Status.InvalidMove:
                                                                Move((Direction)(r.Next() % 4));
                                                                break;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                switch (Move(position.x < nearest.x ? Direction.Right : Direction.Left))
                                                {
                                                    case Status.InvalidMove:
                                                        Move((Direction)(r.Next() % 4));
                                                        break;
                                                }
                                            }
                                        }
                                        else if (nearest.y != position.y)
                                        {
                                            switch (Move(position.y < nearest.y ? Direction.Up : Direction.Down))
                                            {
                                                case Status.InvalidMove:
                                                    Move((Direction)(r.Next() % 4));
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Destroy();
                                }
                            }
                            else // carried piece == null
                            {
                                //move to task area
                                if (position.y < goalAreaHeight)
                                {
                                    switch (Move(Direction.Up))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                else if (position.y >= map.GetLength(1) - goalAreaHeight)
                                {
                                    switch (Move(Direction.Down))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }

                                //find a piece

                                //if on it - pick
                                else if (map[position.x, position.y].distanceToPiece == 0)
                                {
                                    //if is 'middle player' and found piece closer to rivals' goal area just stand there to confuse enemies
                                    if (role == -1 && ((team == 0 && position.y >= map.GetLength(1) * 3 / 4) || (team == 1 && position.y <= map.GetLength(1) / 4)))
                                        Discover();
                                    else
                                        Pick();
                                }

                                //if you could be closer - go there
                                //todo pick the best from adjacent fields
                                else if (position.x + 1 < map.GetLength(0)
                                    && map[position.x + 1, position.y].distanceToPiece > -1
                                    && map[position.x + 1, position.y].distanceToPiece < map[position.x, position.y].distanceToPiece
                                    && map[position.x + 1, position.y].timestamp > map[position.x, position.y].timestamp - movemult * basepenalty * 10)
                                {
                                    switch (Move(Direction.Right))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                else if (map[position.x, position.y + 1].distanceToPiece > -1
                                    && map[position.x, position.y + 1].distanceToPiece < map[position.x, position.y].distanceToPiece
                                    && map[position.x, position.y + 1].timestamp > map[position.x, position.y].timestamp - movemult * basepenalty * 10)
                                {
                                    if (position.y + 1 >= map.GetLength(1) - goalAreaHeight && discmult < 3 * movemult)
                                    {
                                        //wrong info, field in goal area can't be closer to a piece
                                        switch (Discover())
                                        {

                                        }
                                    }
                                    else
                                    {
                                        switch (Move(Direction.Up))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                }
                                else if (map[position.x, position.y - 1].distanceToPiece > -1
                                    && map[position.x, position.y - 1].distanceToPiece < map[position.x, position.y].distanceToPiece
                                    && map[position.x, position.y - 1].timestamp > map[position.x, position.y].timestamp - movemult * basepenalty * 10)
                                {
                                    if (position.y <= goalAreaHeight && discmult < 3 * movemult)
                                    {
                                        //wrong info, field deeper in goal area can't be closer to a piece
                                        switch (Discover())
                                        {

                                        }
                                    }
                                    else
                                    {
                                        switch (Move(Direction.Down))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                }
                                else if (position.x > 0
                                    && map[position.x - 1, position.y].distanceToPiece > -1
                                    && map[position.x - 1, position.y].distanceToPiece < map[position.x, position.y].distanceToPiece
                                    && map[position.x - 1, position.y].timestamp > map[position.x, position.y].timestamp - movemult * basepenalty * 10)
                                {
                                    switch (Move(Direction.Left))
                                    {
                                        case Status.InvalidMove:
                                            Move((Direction)(r.Next() % 4));
                                            break;
                                    }
                                }
                                //if no pieces go to a waiting spot
                                else if (map[position.x, position.y].distanceToPiece == -1)
                                {
                                    //horizontal positioning
                                    if (position.x > prefferedColumn)
                                    {
                                        switch (Move(Direction.Left))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                    else if (position.x < prefferedColumn)
                                    {
                                        switch (Move(Direction.Right))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                    //vertical positioning
                                    else if (position.y < prefferedRow)
                                    {
                                        switch (Move(Direction.Up))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                    else if (position.y > prefferedRow)
                                    {
                                        switch (Move(Direction.Down))
                                        {
                                            case Status.InvalidMove:
                                                Move((Direction)(r.Next() % 4));
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        switch (Discover())
                                        {

                                        }
                                    }
                                }
                                else
                                {
                                    //all adjacent fields are thought to be further or at same distance from piece as the field we are on
                                    //so there is either no piece on board, or our current info is bad
                                    //also our distance info may be too old (>10 moves)


                                    //assuming our info is bad we go to the field that was updated earlier

                                    if (discmult > 8 * movemult)
                                    {
                                        Status s;
                                        directions.Clear();
                                        if (position.x > 0)
                                        {
                                            directions.Add((map[position.x - 1, position.y].timestamp, Direction.Left));
                                        }
                                        if (position.x + 1 < map.GetLength(0))
                                        {
                                            directions.Add((map[position.x + 1, position.y].timestamp, Direction.Right));
                                        }
                                        if (position.y + 1 < map.GetLength(1) - goalAreaHeight)
                                        {
                                            directions.Add((map[position.x, position.y + 1].timestamp, Direction.Up));
                                        }
                                        if (position.y > goalAreaHeight)
                                        {
                                            directions.Add((map[position.x, position.y - 1].timestamp, Direction.Down));
                                        }
                                        directions.Sort((d1, d2) => d1.time.CompareTo(d2.time));

                                        foreach (var v in directions)
                                        {
                                            s = Move(v.d);
                                            if (s == Status.OK)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        switch (Discover())
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                myLogForm.Log("Farewell!");
                myLogForm.DisplayFull();
                Thread.Sleep(1000);
                connection.Close();
                connection.Dispose();
                //stream.close()
                //client.close()
                if (logWhenGameIsOver)
                    myLogForm.WriteTextToFile("team_" + team + "_id_" + id + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".txt");
                //myLogForm.Invoke((MethodInvoker)delegate { myLogForm.Close(); }); // not sure about this, maybe just let the user close the form
            }
            catch (EndGameException)
            {
                myLogForm.Log("Farewell!");
                myLogForm.DisplayFull();
                Thread.Sleep(1000);
                connection.Close();
                connection.Dispose();
                //stream.close()
                //client.close()
                if (logWhenGameIsOver)
                    myLogForm.WriteTextToFile("team_" + team + "_id_" + id + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".txt");
            }
        }

        /// <summary>
        /// Szukanie najbliżej położonego niesprawdzonego pola w strefie celu (goal area)
        /// </summary>
        /// <param name="position"></param>
        /// <returns> Współrzędne najbliższego niesprawdzonego pola w strefie celu (goal area) </returns>
        private (int x, int y) FindNearestPossibleGoal((int x, int y) position)
        {
            var n = possibleGoalFieldsCoordinates.Aggregate((c1, c2) => Manhattan(c1, position) < Manhattan(c2, position) ? c1 : c2);
            if (Manhattan(n, position) == int.MaxValue)
            {
                throw new ApplicationException("No more unchecked fields and the game has not ended yet");
            }
            return (n.x, n.y);
        }

        /// <summary>
        /// Obliczanie odległości miejskiej (Manhattan) między dwoma punktami: <paramref name="field"/> <paramref name="point"/>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private int Manhattan((int x, int y) field, (int x, int y) point)
        {
            if (map[field.x, field.y].isGoal.HasValue) return int.MaxValue;
            return Math.Abs(field.x - point.x) + Math.Abs(field.y - point.y);
        }
        /// <summary>
        /// Akcja podniesienia kawałka przez agenta )
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Pick()
        {
            myLogForm.Log("Trying to pick up piece");

            string responseJson = SendMessageAndWaitForResponse(
            "{\n" +
                "\"msgId\": 66,\n" +
                "\"agentId\": " + id + "\n" +
            "}", MessageType.PickPieceRequest);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 130)
            {
                //something went wrong
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            carriedPiece = new Piece();
            map[position.x, position.y].distanceToPiece = -1;
            return Status.OK;
        }
        /// <summary>
        /// Akcja odłożenia kawałka przez agenta
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Put()
        {
            myLogForm.Log("Trying to put piece");

            string responseJson = SendMessageAndWaitForResponse(
            "{\n" +
                "\"msgId\": 69,\n" +
                "\"agentId\": " + id + "\n" +
            "}", MessageType.PutPieceRequest);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 133)
            {
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            switch ((int)response.result)
            {
                case 0:
                    myLogForm.Log("Put piece in task area");
                    map[position.x, position.y].distanceToPiece = 0;
                    break;
                case 1: 
                    myLogForm.Log("Found goal");
                    map[position.x, position.y].isGoal = true;
                    possibleGoalFieldsCoordinates.Remove(position);
                    break;
                case 2: 
                    myLogForm.Log("Found not goal");
                    map[position.x, position.y].isGoal = false;
                    possibleGoalFieldsCoordinates.Remove(position);
                    break;
                case 3: 
                    myLogForm.Log("Put fake piece in goal area");
                    break;
                default:
                    throw new JsonException("Invalid result value");
            }
            carriedPiece = null;
            return Status.OK;
        }
        /// <summary>
        /// Akcja zniszczenia kawałka przez agenta
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Destroy()
        {
            myLogForm.Log("Trying to destroy piece");

            string responseJson = SendMessageAndWaitForResponse(
            "{\n" +
                "\"msgId\": 68,\n" +
                "\"agentId\": " + id + "\n" +
            "}", MessageType.DestroyPieceRequest);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 132)
            {
                //something went wrong
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            carriedPiece = null;
            return Status.OK;
        }
        /// <summary>
        /// Akcja skanowania pól wokół agenta (3x3)
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Discover()
        {
            myLogForm.Log("Trying to discover");

            string responseJson = SendMessageAndWaitForResponse(
            "{\n" +
                "\"msgId\": 65,\n" +
                "\"agentId\": " + id + "\n" +
            "}", MessageType.Discover3x3Request);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 129)
            {
                //something went wrong
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            int t = response.timestamp;
            List<dynamic> discovery = response.closestPieces.ToObject<List<dynamic>>();
            foreach (var v in discovery)
            {
                map[v.x, v.y].timestamp = t;
                map[v.x, v.y].distanceToPiece = v.dist;
            }
            int current = map[position.x, position.y].distanceToPiece;
            if (current > 0)
            {
                bool edgeLeft = position.x == 0;
                bool edgeRight = position.x + 1 == map.GetLength(0);
                bool edgeTop = position.y + 1 == map.GetLength(1) - goalAreaHeight;
                bool edgeBottom = position.y == goalAreaHeight;

                //piece up?
                if (position.y < map.GetLength(1) - goalAreaHeight - 2 && map[position.x, position.y + 1].distanceToPiece < current 
                    && (edgeLeft  || map[position.x - 1, position.y + 1].distanceToPiece == current)
                    && (edgeRight || map[position.x + 1, position.y + 1].distanceToPiece == current))
                {
                    for (int i = 2; i <= current; i++)
                    {
                        int k = current - i;
                        int j = position.y + i;
                        map[position.x, j].distanceToPiece = k;
                        map[position.x, j].timestamp = t;
                        k++;
                        if (!edgeLeft)
                        {
                            map[position.x - 1, j].distanceToPiece = k;
                            map[position.x - 1, j].timestamp = t;
                        }
                        if (!edgeRight)
                        {
                            map[position.x + 1, j].distanceToPiece = k;
                            map[position.x + 1, j].timestamp = t;
                        }
                    }
                }
                //piece right?
                if (position.x < map.GetLength(0) - 2 && map[position.x + 1, position.y].distanceToPiece < current 
                    && (edgeBottom || map[position.x + 1, position.y - 1].distanceToPiece == current)
                    && (edgeTop    || map[position.x + 1, position.y + 1].distanceToPiece == current))
                {
                    for (int i = 2; i <= current; i++)
                    {
                        int k = current - i;
                        int j = position.x + i;
                        map[j, position.y].distanceToPiece = k;
                        map[j, position.y].timestamp = t;
                        k++;
                        if (!edgeBottom)
                        {
                            map[j, position.y - 1].distanceToPiece = k;
                            map[j, position.y - 1].timestamp = t;
                        }
                        if (!edgeTop)
                        {
                            map[j, position.y + 1].distanceToPiece = k;
                            map[j, position.y + 1].timestamp = t;
                        }
                    }
                }
                //piece left?
                if (position.x > 1 && map[position.x - 1, position.y].distanceToPiece < current
                    && (edgeBottom || map[position.x - 1, position.y - 1].distanceToPiece == current)
                    && (edgeTop    || map[position.x - 1, position.y + 1].distanceToPiece == current))
                {
                    for (int i = 2; i <= current; i++)
                    {
                        int k = current - i;
                        int j = position.x - i;
                        map[j, position.y].distanceToPiece = k;
                        map[j, position.y].timestamp = t;
                        k++;
                        if (!edgeBottom)
                        {
                            map[j, position.y - 1].distanceToPiece = k;
                            map[j, position.y - 1].timestamp = t;
                        }
                        if (!edgeTop)
                        {
                            map[j, position.y + 1].distanceToPiece = k;
                            map[j, position.y + 1].timestamp = t;
                        }
                    }
                }
                //piece down?
                if (position.y > goalAreaHeight + 1 && map[position.x, position.y - 1].distanceToPiece < current
                    && (edgeLeft  || map[position.x - 1, position.y - 1].distanceToPiece == current)
                    && (edgeRight || map[position.x + 1, position.y - 1].distanceToPiece == current))
                {
                    for (int i = 2; i <= current; i++)
                    {
                        int k = current - i;
                        int j = position.y - i;
                        map[position.x, j].distanceToPiece = k;
                        map[position.x, j].timestamp = t;
                        k++;
                        if (!edgeLeft)
                        {
                            map[position.x - 1, j].distanceToPiece = k;
                            map[position.x - 1, j].timestamp = t;
                        }
                        if (!edgeRight)
                        {
                            map[position.x + 1, j].distanceToPiece = k;
                            map[position.x + 1, j].timestamp = t;
                        }
                    }
                }
            }
            return Status.OK;
        }
        /// <summary>
        /// Akcja sprawdzenia kawałka przez agenta
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status CheckPiece()
        {
            myLogForm.Log("Trying to check piece");

            string responseJson = SendMessageAndWaitForResponse(
            "{\n" +
                "\"msgId\": 67,\n" +
                "\"agentId\": " + id + "\n" +
            "}", MessageType.CheckPieceRequest);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 131)
            {
                //something went wrong
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            carriedPiece.real = response.isCorrect;
            return Status.OK;
        }
        /// <summary>
        /// Akcja ruchu agenta w wybranym kierunku <paramref name="d"/>
        /// </summary>
        /// <param name="d"></param>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Move(Direction d)
        {
            myLogForm.Log("Trying to move in direction " + d.ToString());

            string responseJson = SendMessageAndWaitForResponse(new MakeMoveRequestMessage
            {
                moveDirection = d
            }.ToJson(), MessageType.MakeMoveRequest);

            dynamic response = JsonConvert.DeserializeObject(responseJson);

            if (response.msgId != 128)
            {
                //something went wrong
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 5:
                        myLogForm.Log("Invalid move");
                        return Status.InvalidMove;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            switch (d)
            {
                case Direction.Up:
                    myLogForm.Log("Moved up");
                    position.y++;
                    break;
                case Direction.Right:
                    myLogForm.Log("Moved right");
                    position.x++;
                    break;
                case Direction.Down:
                    myLogForm.Log("Moved down");
                    position.y--;
                    break;
                case Direction.Left:
                    myLogForm.Log("Moved left");
                    position.x--;
                    break;
            }
            map[position.x, position.y].timestamp = response.timestamp;
            map[position.x, position.y].distanceToPiece = response.closestPiece;
            myLogForm.Log("Closest piece distance is " + response.closestPiece.ToString());
            return Status.OK;
        }

        /// <summary>
        /// Akcja komunikacji agenta wysyłana do lidera drużyny
        /// </summary>
        /// <returns> Zaktualizowany stan (w trakcie kary, nieprawidłowa akcja, gra jeszcze się nie rozpoczęła, OK) </returns>
        Status Communicate()
        {
            myLogForm.Log("Trying to communicate with " + leaderId);
            expecting_data = true;
            bool? prev = map[position.x, position.y].isGoal;
            map[position.x, position.y].isGoal = false;
            string s = JsonConvert.SerializeObject(map);
            map[position.x, position.y].isGoal = prev;
            string responseJson = SendMessageAndWaitForResponse(new ExchangeInformationWithDataRequest
            {
                withAgentId = leaderId,
                data = s
            }.ToJson(), MessageType.ExchangeInformationWithDataRequest);
            if (responseJson != null)
            {
                //something went wrong
                dynamic response = JsonConvert.DeserializeObject(responseJson);
                switch ((int)response.msgId)
                {
                    case 0:
                        throw new JsonException("Json we sent is considered to be invalid");
                    case 1:
                        throw new ApplicationException("GM not responding");
                    case 4:
                        myLogForm.Log("Not so fast cowboy");
                        return Status.RequestDuringTimePenalty;
                    case 5:
                        myLogForm.Log("Invalid move");
                        return Status.InvalidMove;
                    case 6:
                        return Status.NoGameYet;
                    case 7:
                        myLogForm.Log("Invalid action");
                        return Status.InvalidAction;
                    default:
                        throw new JsonException("Unexpected message type");
                }
            }
            return Status.OK;
        }

        /// <summary>
        /// Łączenie potencjalnego agenta z serwerem o adresie IP <paramref name="address"/> na porcie <paramref name="port"/>
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <returns> Rezultat połączenia - sukces lub porażkę </returns>
        bool Connect(string address, int port)
        {
            try
            {
                connection = new TcpClient(address, port);
            }
            catch (SocketException se)
            {
                string msg = "Something went wrong\nErrorCode: " + se.ErrorCode + "\n" + se.Message;
                switch (se.SocketErrorCode)
                {
                    case SocketError.HostNotFound:
                        msg = "HostNotFound";
                        break;
                    case SocketError.TimedOut:
                        msg = "The connection attempt timed out, or the connected host has failed to respond.";
                        break;
                    case SocketError.ConnectionRefused:
                        msg = "The remote host is actively refusing the connection.";
                        break;

                }
                MessageBox.Show(msg, "SocketException", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show(ae.Message, "Invalid argument", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Dekodowanie json'a <paramref name="message"/>
        /// </summary>
        /// <param name="message"></param>
        /// <returns> Pusta wiadomość </returns>
        public string ReceiveRequest(string message)
        {
            Decode(message);
            return string.Empty;
        }
        /// <summary>
        /// Oczekiwanie na początek gry - czytanie pojedynczej wiadomości
        /// </summary>
        void WaitForGameStart()
        {
            var msg = connection.GetSingleMessage();
            Decode(msg);
        }
        /// <summary>
        /// Dekodowanie i przetwarzanie wiadomości z <paramref name="json"/>'a
        /// </summary>
        /// <param name="json"></param>
        void Decode(string json)
        {
            dynamic request = JsonConvert.DeserializeObject(json);
            switch ((int)request.msgId)
            {
                case 1:     // GM not responding
                    End();
                    break;

                case 32:
                    goalAreaHeight = request.goalAreaHeight;
                    // boardSize - with Goal Area
                    // What to put into map?
                    map = new Field[request.boardSizeX, request.boardSizeY + 2 * goalAreaHeight];
                    for (int i = 0; i < map.GetLength(0); i++)
                        for (int j = 0; j < map.GetLength(1); j++)
                            map[i, j] = new Field();
                    for (int i = 0; i < map.GetLength(0); i++)
                    {
                        if (team == 0)
                        {
                            for (int j = 0; j < goalAreaHeight; j++)
                                possibleGoalFieldsCoordinates.Add((i, j));
                        }
                        else
                        {
                            for (int j = map.GetLength(1) - goalAreaHeight; j < map.GetLength(1); j++)
                                possibleGoalFieldsCoordinates.Add((i, j));
                        }
                    }
                    position = (request.initialXPosition, request.initialYPosition);
                    id = request.agentId;
                    leaderId = request.teamLeaderId;
                    isLeader = id == leaderId;
                    basepenalty = (long)request.baseTimePenalty;
                    movemult = (int)request.tpm_move;
                    discmult = (int)request.tpm_discoverPieces;
                    chckmult = (int)request.tpm_checkPiece;
                    commmult = (int)request.tpm_infoExchange;
                    dstrmult = (int)request.tpm_destroyPiece;
                    putmult  = (int)request.tpm_putPiece;
                    misfortune = (double)request.probabilityOfBadPiece;
                    ids = (request.agentIdsFromTeam as Newtonsoft.Json.Linq.JArray).ToObject<int[]>();
                    if (ids.Length > map.GetLength(0))
                    {
                        foreach (int i in ids)
                        {
                            if (i > id)
                            {
                                role++;
                            }
                        }
                        block = true;
                        if(role >= map.GetLength(0))
                        {
                            role -= map.GetLength(0);
                            double d = (role + 0.5) * map.GetLength(0) / (ids.Length - map.GetLength(0));
                            prefferedColumn = Convert.ToInt32(d);
                            if (team == 0)
                                prefferedRow = (map.GetLength(1) - 1) / 2;
                            else
                                prefferedRow = map.GetLength(1) / 2;
                        }
                        else
                        {
                            prefferedColumn = role;
                            role = -2;
                            prefferedRow = team == 0 ? map.GetLength(1) - goalAreaHeight - 1 : goalAreaHeight;
                        }

                    }
                    else
                    {
                        foreach (int i in ids)
                        {
                            if (i > id)
                            {
                                role++;
                            }
                        }
                        double d = (role + 0.5) * map.GetLength(0) / ids.Length;
                        prefferedColumn = Convert.ToInt32(d);
                        if (role == ids.Length / 2) role = -1;
                        //prefferedColumn = role * (map.GetLength(0) - 1) / (ids.Length - 1);
                        if (team == 0)
                            prefferedRow = role == -1 ? map.GetLength(1) - goalAreaHeight - 1 : (map.GetLength(1) - 1) / 2;
                        else
                            prefferedRow = role == -1 ? goalAreaHeight : map.GetLength(1) / 2;
                    }
                    myLogForm.Invoke((MethodInvoker)delegate
                    {
                        myLogForm.Text += ", id " + id;
                    });
                    Task t = new Task(Run);
                    t.Start();
                    break;

                case 33:
                    End();
                    break;
            }
        }
        /// <summary>
        /// Wysłanie wiadomości <paramref name="message"/> typu <paramref name="msgId"/> i oczekiwanie na odpowiedź
        /// </summary>
        /// <param name="message"></param>
        /// <param name="msgId"></param>
        /// <returns></returns>
        private string SendMessageAndWaitForResponse(string message, MessageType msgId)
        {
            connection.SendMessage(message, msgId);
            var response = connection.GetSingleMessage();
            Decode(response);
            SynchronizeTime(response);
            dynamic request = JsonConvert.DeserializeObject(response);
            while (request.msgId == 134)
            {
                myLogForm.Log("Got data from other player");

                if ((bool)request.agreement)
                {
                    Field[,] tmpMap = JsonConvert.DeserializeObject<Field[,]>((string)request.data);
                    for (int i = 0; i < tmpMap.GetLength(0); i++)
                        for (int j = 0; j < tmpMap.GetLength(1); j++)
                        {
                            if (tmpMap[i, j].isGoal != null && map[i, j].isGoal == null)
                            {
                                possibleGoalFieldsCoordinates.Remove((i, j));
                                map[i, j] = tmpMap[i, j];
                            }
                            else if (tmpMap[i, j].timestamp > map[i, j].timestamp)
                            {
                                map[i, j] = tmpMap[i, j];
                            }
                        }
                }

                if (expecting_data)
                {
                    expecting_data = false;
                    return null;
                }
                response = connection.GetSingleMessage();
                //handle gameOver i inne 'bezwzgledne wiadomosci'
                Decode(response);
                SynchronizeTime(response);
                request = JsonConvert.DeserializeObject(response);
            }
            if (request.msgId == 71)
            {
                myLogForm.Log("Accepting communication request");
                return SendMessageAndWaitForResponse(new ExchangeInformationWithDataAgreement
                {
                    agreement = true,
                    withAgentId = request.withAgentId,
                    data = JsonConvert.SerializeObject(map)
                }.ToJson(), MessageType.ExchangeInformationWithDataAgreement);
            }
            if (request.msgId == 71)
                Console.WriteLine();
            return response;
        }
        /// <summary>
        /// Aktualizacja czasu agenta, po którym będzie mógł znowu się ruszyć
        /// </summary>
        /// <param name="response"></param>
        private void SynchronizeTime(string response)
        {
            try
            {
                dynamic model = JsonConvert.DeserializeObject(response);
                _timer.SetTime((long)model.timestamp);
                if (model.msgId != 5 && model.msgId != 7 && model.msgId != 49)
                {
                    _nextMoveTime = (long)model.waitUntilTime;
                }
            }
            catch (RuntimeBinderException e)
            {
                //swallow
                myLogForm?.Log("RuntimeBinderException");
            }
        }
        /// <summary>
        /// Zakończenie pracy agenta
        /// </summary>
        void End()
        {
            isGameRunning = false;
            throw new EndGameException();
        }
        /// <summary>
        /// Włączenie/wyłączenie logowania do pliku po zakończeniu pracy agenta w zależności od flagi <paramref name="logOption"/>
        /// </summary>
        /// <param name="logOption"></param>
        public void Logging(bool logOption)
        {
            logWhenGameIsOver = logOption;
        }
        /// <summary>
        /// Reprezentacja odłamka
        /// </summary>
        class Piece
        {
            public bool? real = null;
        }
        /// <summary>
        /// Reprezentacja pola na mapie gry
        /// </summary>
        class Field
        {
            public int distanceToPiece = -1;
            public bool? isGoal;
            public long timestamp = 0;
        }
        /// <summary>
        /// Zegar do odmierzania kar czasowych
        /// </summary>
        class Timer : Stopwatch
        {
            private long offset;

            public void SetTime(long time)
            {
                offset = time - base.ElapsedMilliseconds;
            }

            public void SleepUntilTime(long time)
            {
                var sleepTime = time - ElapsedMilliseconds;
                if (sleepTime <= 0)
                {
                    return;
                }
                Thread.Sleep((int)sleepTime);
            }

            public new long ElapsedMilliseconds => base.ElapsedMilliseconds + offset;
        }

        /// <summary>
        /// Stany, po których agent jest w stanie kontynuować grę (lub oczekiwać na jej rozpoczęcie)
        /// </summary>
        enum Status
        {
            OK, AgentNotResponding, RequestDuringTimePenalty, InvalidMove, InvalidAction, NoGameYet
        }
    }

    /// <summary>
    /// Wyjątek rzucany w momencie zakończenia gry
    /// </summary>
    [Serializable]
    internal class EndGameException : Exception
    {
        public EndGameException()
        {
        }

        public EndGameException(string message) : base(message)
        {
        }

        public EndGameException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EndGameException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    /// <summary>
    /// Możliwe kierunki ruchu agentów
    /// </summary>
    public enum Direction
    {
        Up, Right, Down, Left
    }

}
