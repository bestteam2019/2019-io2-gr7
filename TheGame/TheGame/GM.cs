﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheGame.Abstract;
using TheGame.CommunicationServerModule;

// Reverting changes

namespace TheGame
{
    /// <summary>
    /// Obiekt Mistrza Gry (GM) nadzorującego przebieg rozgrywki
    /// </summary>
    public class GM
    {
        #region time
        private readonly Stopwatch _stopwatch;
        private long Timestamp => _stopwatch.ElapsedMilliseconds;
        private readonly long _baseTimePenalty;
        private readonly Dictionary<MessageType, int> _timePenaltyMultipliers;
        private readonly Dictionary<int, long> _nextMoveTime = new Dictionary<int, long>();
        #endregion

        private int goalAreaHeight;
        private int goals0;
        private int goals1;
        private int players0 = 0;
        private int players1 = 0;
        private int numberOfPlayers;
        private int pieceSpawnTime;
        private int maxPieces;
        private int currentBoardPieces; // doesn't count the pieces held by players - needed for efficiently spawning new pieces
        private double badPieceChance;
        private bool areTeamsFull;
        private TcpClient connection;
        private Random r = new Random();
        private Field[,] map;
        private Dictionary<int, PlayerInfo> players;
        private DateTime timeOfLastPieceSpawn;
        private HashSet<Piece> pieces = new HashSet<Piece>();
        private GameView myGameView;
        private int refreshCompare;
        private bool playing;
        private Dictionary<int, string> waiting = new Dictionary<int, string>();
        private readonly HashSet<int> _playersThatHaveToRespondToDataExchange = new HashSet<int>();
        private readonly bool _testMode;
        public bool CommunicationWithEnemiesAllowed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"> Adres IP serwera komunikacyjnego </param>
        /// <param name="port"> Port, na którym znajduje się serwer komunikacyjny </param>
        /// <param name="height"> Wysokość mapy gry </param>
        /// <param name="width"> Szerokość mapy gry </param>
        /// <param name="goalAreaHeight"> Wysokość strefy celu (goal area) </param>
        /// <param name="goals"> Liczba celi do spełnienia przez każdą z drużyn </param>
        /// <param name="players"> Liczba graczy uczestniczących w grze </param>
        /// <param name="pieceInterval"> Przerwa między pojawieniem się kolejnych kawałków (ms) </param>
        /// <param name="maxP"> Maksymalna liczba kawałków na mapie gry </param>
        /// <param name="badPieceChance"> Szansa na wadliwy (fałszywy) kawałek z przedziału [0, 1] </param>
        /// <param name="baseTimePenaltyInMilis"> Podstawowy czas kar (ms) </param>
        /// <param name="move"> Mnożnik kary czasowej za próbę ruchu (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="discover"> Mnożnik kary czasowej za próbę skanu (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="pick"> Mnożnik kary czasowej za próbę podniesienia odłamka (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="check"> Mnożnik kary czasowej za próbę sprawdzenia odłamka (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="destroy"> Mnożnik kary czasowej za próbę zniszczenia odłamka (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="put"> Mnożnik kary czasowej za próbę odłożenia odłamka (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="exchange">  Mnożnik kary czasowej za próbę komunikacji z innym graczem (*<paramref name="baseTimePenaltyInMilis"/>) </param>
        /// <param name="showGui"> Flaga określająca wyświetlanie interfejsu graficznego od początku rozgrywki </param>
        /// <param name="testMode"> Flaga określająca, czy uruchomiono Mistrza Gry (GM) w trybie testowym </param>
        public GM(string address, int port, int height, int width, int goalAreaHeight, int goals, int players,
            int pieceInterval, int maxP, double badPieceChance,
            int baseTimePenaltyInMilis, int move, int discover, int pick, int check, int destroy, int put, int exchange,
            bool showGui = false, bool testMode = false)
        {
            _testMode = testMode;
            if (!_testMode && !Connect(address, port)) throw new SocketException(11002);
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
            map = new Field[width, height + 2 * goalAreaHeight];
            for (int i = 0; i < map.GetLength(0); i++)
                for (int j = 0; j < map.GetLength(1); j++)
                    map[i, j] = new Field();
            maxPieces = maxP;
            currentBoardPieces = 0;
            this.goalAreaHeight = goalAreaHeight;
            goals0 = goals1 = goals;
            numberOfPlayers = players;
            pieceSpawnTime = pieceInterval;
            this.badPieceChance = badPieceChance;
            _baseTimePenalty = baseTimePenaltyInMilis;
            _timePenaltyMultipliers = new Dictionary<MessageType, int>
            {
                [MessageType.MakeMoveRequest] = move,
                [MessageType.Discover3x3Request] = discover,
                [MessageType.PickPieceRequest] = pick,
                [MessageType.DestroyPieceRequest] = destroy,
                [MessageType.PutPieceRequest] = put,
                [MessageType.ExchangeInformationRequest] = exchange,
                [MessageType.ExchangeInformationWithDataRequest] = exchange,
                [MessageType.ExchangeInformationWithDataResponse] = exchange,
                [MessageType.CheckPieceRequest] = check
            };

            this.players = new Dictionary<int, PlayerInfo>(players);

            if (_testMode)
            {
                return;
            }

            var request = new GMJoinToGameRequestMessage().ToJson();
            var response = SendMessageAndWaitForResponse(request, MessageType.GMJoinToGameRequest);
            var decodedResponse = MessageModel.Decode(response) as GMJoinToGameResponseMessage;
            if (decodedResponse == null) throw new JsonException("Unexpected response from server");
            if (!decodedResponse.isConnected) throw new ApplicationException("Unable to join a game on server");

            //dynamic response = 
            //    JsonConvert.DeserializeObject(connection.SendRequestToServer(
            //    "{\n" +
            //    "\"msgId\": 50,\n" +
            //    "}"));
            //if (response.msgId != 51) throw new JsonException("Unexpected response from server");
            //if (!(bool)response.isConnected) throw new ApplicationException("Unable to create a game on server");
            if (showGui)
            {
                refreshCompare = -1;
                myGameView = new GameView(goalAreaHeight, height, width, maxPieces, badPieceChance, players);
                myGameView.Show();
            }

            DisplayMap();
            var processingTask = new Task(ProcessRequests);
            processingTask.Start();
        }

        /// <summary>
        /// Przetwarzanie żądań przekierowanych przez serwer od agentów i generowanie odpowiedzi na nie
        /// </summary>
        private void ProcessRequests()
        {
            while (true)
            {
                var request = connection.GetSingleMessage();
                var decodedRequest = MessageModel.Decode(request);
                var response = ReceiveRequest(request, out var startGame, out MessageType msgId); //todo refactoring, maybe can work on model, not string
                if (response != null)
                {
                    SendResponse(response, msgId);
                }

                if (startGame)
                {
                    StartGame();
                }
            }
        }
        
        /// <summary>
        /// Dekodowanie wiadomości <paramref name="json"/> i zapisywanie jej typu w <paramref name="msgId"/>
        /// </summary>
        /// <exception cref="JsonException"/>
        public string ReceiveRequest(string json, out bool startGame, out MessageType msgId)
        {
            startGame = false;
            dynamic request = JsonConvert.DeserializeObject(json);
            int id = request.agentId;
            int i;
            if (playing)
            {
                SpawnPiece();
            }
            if (goals0 == 0 || goals1 == 0)
            {
                msgId = MessageType.InvalidAction;
                return "{\n" +
                                "\"msgId\": 7,\n" +
                                "\"agentId\": " + id + "\n" +
                            "}";
            }

            var decodedRequest = MessageModel.Decode(json) as MessageModelWithAgentId;
            if (decodedRequest != null
                && _playersThatHaveToRespondToDataExchange.Contains(decodedRequest.agentId)
                && decodedRequest.msgId != MessageType.ExchangeInformationWithDataAgreement)
            {
                msgId = MessageType.InvalidAction;
                return new InvalidActionMessage
                {
                    agentId = decodedRequest.agentId,
                    timestamp = Timestamp
                }.ToJson();
            }

            switch ((int)request.msgId)
            {

                case 48:
                    bool isConnected = false;
                    if (!areTeamsFull)
                    {
                        PlayerInfo newPlayer = new PlayerInfo
                        {
                            id = id,
                            isLeader = false,
                            team = request.teamId
                        };
                        if (request.teamId == 0 && players0 < numberOfPlayers / 2)
                        {
                            if (players0 == 0)
                                newPlayer.isLeader = true;
                            players.Add(id, newPlayer);
                            isConnected = true;
                            players0++;
                            if (players0 == numberOfPlayers / 2 && players1 == numberOfPlayers / 2)
                                areTeamsFull = true;
                        }
                        else if (request.teamId == 1 && players1 < numberOfPlayers / 2)
                        {
                            if (players1 == 0)
                                newPlayer.isLeader = true;
                            players.Add(id, newPlayer);
                            isConnected = true;
                            players1++;
                            if (players0 == numberOfPlayers / 2 && players1 == numberOfPlayers / 2)
                                areTeamsFull = true;
                        }

                        UpdateTitle();

                        if (areTeamsFull)
                        {
                            startGame = true;
                        }
                    }

                    msgId = MessageType.JoinToGameResponse;
                    return "{\n" +
                           "\"msgId\": 49,\n" +
                           "\"agentId\": " + id + ",\n" +
                           "\"isConnected\": " + isConnected.ToString().ToLower() + "\n" +
                           "}";
                case 64:
                    i = Move(id, (Direction)request.moveDirection);
                    DisplayMap();
                    switch (i)
                    {
                        case 0:
                            int dist = -1;
                            if (pieces.Count > 0)
                            {
                                dist = pieces.Min(piece => Manhattan(piece, players[id].position));
                                if (dist == int.MaxValue)
                                {
                                    dist = -1;
                                }
                            }

                            msgId = MessageType.MakeMoveResponse;
                            return new MakeMoveResponseMessage
                            {
                                agentId = id,
                                timestamp = Timestamp,
                                waitUntilTime = WaitUntilTime(MessageType.MakeMoveRequest, id),
                                closestPiece = dist
                            }.ToJson();
                        case 4:
                            msgId = MessageType.RequestDuringTimePenaltyResponse;
                            return new RequestDuringTimePenaltyMessage
                            {
                                agentId = id,
                                timestamp = Timestamp,
                                waitUntilTime = WaitUntilTime(MessageType.RequestDuringTimePenaltyResponse, id)
                            }.ToJson();
                        case 5:
                            msgId = MessageType.CantMoveInThisDirection;
                            return new CantMoveMessage
                            {
                                agentId = id,
                                timestamp = Timestamp
                            }.ToJson();
                        case 7:
                            msgId = MessageType.InvalidAction;
                            return new InvalidActionMessage
                            {
                                agentId = id,
                                timestamp = Timestamp
                            }.ToJson();
                    }

                    msgId = (MessageType)i;
                    return
                        "{\n" +
                        "\"msgId\": " + i + ",\n" +
                        "\"agentId\": " + id + ",\n" +
                        "\"timestamp\": " + Timestamp + "\n" +
                        "}";
                case 65:
                    i = DiscoverField(id);
                    DisplayMap();
                    if (i == 4)
                    {
                        msgId = MessageType.RequestDuringTimePenaltyResponse;
                        return new RequestDuringTimePenaltyMessage
                        {
                            agentId = id,
                            timestamp = Timestamp,
                            waitUntilTime = WaitUntilTime(MessageType.RequestDuringTimePenaltyResponse, id)
                        }.ToJson();
                    }

                    PlayerInfo p = players[id];
                    List<Discover3x3Response.BlockInfo> discovery = new List<Discover3x3Response.BlockInfo>();
                    for (int x = Math.Max(p.position.x - 1, 0);
                        x <= Math.Min(p.position.x + 1, map.GetLength(0) - 1);
                        x++)
                    {
                        for (int y = Math.Max(p.position.y - 1, 0);
                            y <= Math.Min(p.position.y + 1, map.GetLength(1) - 1);
                            y++)
                        {
                            int dist = -1;
                            if (pieces.Count > 0)
                            {
                                dist = pieces.Min(piece => Manhattan(piece, (x, y)));
                                if (dist == int.MaxValue)
                                {
                                    dist = -1;
                                }
                            }

                            discovery.Add(new Discover3x3Response.BlockInfo
                            {
                                x = x,
                                y = y,
                                dist = dist
                            });
                        }
                    }

                    msgId = MessageType.Discover3x3Response;
                    return new Discover3x3Response
                    {
                        agentId = id,
                        timestamp = Timestamp,
                        waitUntilTime = WaitUntilTime(MessageType.Discover3x3Request, id),
                        closestPieces = discovery.ToArray()
                    }.ToJson();
                case 66:
                    i = PickPiece(id);
                    DisplayMap();
                    if (i == 7)
                    {
                        msgId = MessageType.InvalidAction;
                        return new InvalidActionMessage
                        {
                            agentId = id,
                            timestamp = Timestamp
                        }.ToJson();
                    }

                    if (i == 4)
                    {
                        msgId = MessageType.RequestDuringTimePenaltyResponse;
                        return new RequestDuringTimePenaltyMessage
                        {
                            agentId = id,
                            timestamp = Timestamp,
                            waitUntilTime = WaitUntilTime(MessageType.RequestDuringTimePenaltyResponse, id)
                        }.ToJson();
                    }

                    if (i == 0) i = 130;
                    msgId = MessageType.PickPieceResponse;
                    return new PickPieceResponseMessage
                    {
                        agentId = id,
                        timestamp = Timestamp,
                        waitUntilTime = WaitUntilTime(MessageType.PickPieceRequest, id)
                    }.ToJson();
                case 67:
                    i = CheckPiece(id);
                    DisplayMap();
                    switch (i)
                    {
                        case 0:
                            msgId = MessageType.CheckPieceResponse;
                            return "{\n" +
                                   "\"msgId\": 131,\n" +
                                   "\"agentId\": " + id + ",\n" +
                                   "\"timestamp\": " + Timestamp + ",\n" +
                                   "\"waitUntilTime\": " + WaitUntilTime(MessageType.CheckPieceRequest, id) + ",\n" +
                                   "\"isCorrect\": true\n" +
                                   "}";
                        case 1:
                            msgId = MessageType.CheckPieceResponse;
                            return
                                "{\n" +
                                "\"msgId\": 131,\n" +
                                "\"agentId\": " + id + ",\n" +
                                "\"timestamp\": " + Timestamp + ",\n" +
                                "\"waitUntilTime\": " + WaitUntilTime(MessageType.CheckPieceRequest, id) + ",\n" +
                                "\"isCorrect\": false\n" +
                                "}";
                        case 4:
                            msgId = (MessageType)i;
                            return
                                "{\n" +
                                "\"msgId\": " + i + ",\n" +
                                "\"agentId\": " + id + ",\n" +
                                "\"timestamp\": " + Timestamp + ",\n" +
                                "\"waitUntilTime\": " + WaitUntilTime(MessageType.CheckPieceRequest, id) + "\n" +
                                "}";
                    }

                    msgId = (MessageType)i;
                    return
                        "{\n" +
                        "\"msgId\": " + i + ",\n" +
                        "\"agentId\": " + id + ",\n" +
                        "\"timestamp\": " + Timestamp + "\n" +
                        "}";
                case 68:
                    i = DestroyPiece(id);
                    if (i == 4)
                    {
                        msgId = MessageType.RequestDuringTimePenaltyResponse;
                        return new RequestDuringTimePenaltyMessage
                        {
                            agentId = id,
                            timestamp = Timestamp,
                            waitUntilTime = WaitUntilTime(MessageType.RequestDuringTimePenaltyResponse, id)
                        }.ToJson();
                    }

                    DisplayMap();
                    if (i == 0)
                    {
                        msgId = MessageType.DestroyPieceResponse;
                        return
                            "{\n" +
                            "\"msgId\": 132,\n" +
                            "\"agentId\": " + id + ",\n" +
                            "\"timestamp\": " + Timestamp + ",\n" +
                            "\"waitUntilTime\": " + WaitUntilTime(MessageType.DestroyPieceRequest, id) + "\n" +
                            "}";
                    }

                    msgId = (MessageType)i;
                    return
                        "{\n" +
                        "\"msgId\": " + i + ",\n" +
                        "\"agentId\": " + id + ",\n" +
                        "\"timestamp\": " + Timestamp + "\n" +
                        "}";
                case 69:
                    i = PutPiece(id);
                    if (goals0 != 0 && goals1 != 0)
                        DisplayMap();
                    switch (i)
                    {
                        case 4:
                            msgId = (MessageType)i;
                            return
                                "{\n" +
                                "\"msgId\": " + i + ",\n" +
                                "\"agentId\": " + id + ",\n" +
                                "\"timestamp\": " + Timestamp + ",\n" +
                                "\"waitUntilTime\": " + WaitUntilTime(MessageType.PutPieceRequest, id) + "\n" +
                                "}";
                        case 7:
                            msgId = (MessageType)i;
                            return
                                "{\n" +
                                "\"msgId\": " + i + ",\n" +
                                "\"agentId\": " + id + ",\n" +
                                "\"timestamp\": " + Timestamp + "\n" +
                                "}";
                    }

                    msgId = MessageType.PutPieceResponse;
                    return
                        "{\n" +
                        "\"msgId\": 133,\n" +
                        "\"agentId\": " + id + ",\n" +
                        "\"timestamp\": " + Timestamp + ",\n" +
                        "\"waitUntilTime\": " + WaitUntilTime(MessageType.PutPieceRequest, id) + ",\n" +
                        "\"result\": " + i + "\n" +
                        "}";
                case 70:
                    if (!(decodedRequest is ExchangeInformationWithDataRequest communicationRequest)
                        || !players.ContainsKey(communicationRequest.withAgentId)
                        || (!CommunicationWithEnemiesAllowed && players[communicationRequest.agentId].team !=
                            players[communicationRequest.withAgentId].team))
                    {
                        msgId = MessageType.InvalidAction;
                        return new InvalidActionMessage
                        {
                            agentId = decodedRequest.agentId,
                            timestamp = Timestamp
                        }.ToJson();
                    }

                    if (players[communicationRequest.agentId].isLeader)
                    {
                        _playersThatHaveToRespondToDataExchange.Add(communicationRequest.withAgentId);
                    }

                    waiting.Add(communicationRequest.agentId, communicationRequest.data);

                    msgId = MessageType.ExchangeInformationRequest;
                    return new ExchangeInformationRequest
                    {
                        agentId = communicationRequest.withAgentId,
                        withAgentId = communicationRequest.agentId,
                        timestamp = Timestamp
                    }.ToJson();

                case 72:
                    if (!(decodedRequest is ExchangeInformationWithDataAgreement communicationAgreementWithData)
                        || !waiting.TryGetValue(communicationAgreementWithData.withAgentId, out var waitingData)
                        || (_playersThatHaveToRespondToDataExchange.Contains(communicationAgreementWithData.agentId) && !communicationAgreementWithData.agreement))
                    {
                        msgId = MessageType.InvalidAction;
                        return new InvalidActionMessage
                        {
                            agentId = decodedRequest.agentId,
                            timestamp = Timestamp
                        }.ToJson();
                    }

                    waiting.Remove(communicationAgreementWithData.withAgentId);
                    _playersThatHaveToRespondToDataExchange.Remove(communicationAgreementWithData.agentId);

                    if (communicationAgreementWithData.agreement && !_testMode)
                    {
                        connection.SendMessage(new ExchangeInformationWithDataResponse
                        {
                            agentId = communicationAgreementWithData.agentId,
                            withAgentId = communicationAgreementWithData.withAgentId,
                            agreement = true,
                            timestamp = Timestamp,
                            waitUntilTime = WaitUntilTime(MessageType.ExchangeInformationWithDataResponse, communicationAgreementWithData.agentId),//Done - works ? todo WaitUntilTime(MessageType...., id),
                            data = waitingData
                        }.ToJson(), MessageType.ExchangeInformationWithDataResponse);
                    }
                    msgId = MessageType.ExchangeInformationWithDataResponse;
                    return new ExchangeInformationWithDataResponse
                    {
                        agentId = communicationAgreementWithData.withAgentId,
                        withAgentId = communicationAgreementWithData.agentId,
                        agreement = communicationAgreementWithData.agreement,
                        timestamp = Timestamp,
                        waitUntilTime = WaitUntilTime(MessageType.ExchangeInformationWithDataResponse, communicationAgreementWithData.withAgentId),//Done - works? todo WaitUntilTime(MessageType...., id),
                        data = communicationAgreementWithData.agreement ? communicationAgreementWithData.data : string.Empty
                    }.ToJson();
            }
            throw new JsonException("Unexpected msgId");
        }

        /// <summary>
        /// Zaktualizowanie ilości graczy, którzy są podłączeni do gry w tytule okna Mistrza Gry (GM)
        /// </summary>
        private void UpdateTitle()
        {
            myGameView?.UpdateTitle(players0, players1);
        }

        /// <summary>
        /// Obliczanie odległości miejskiej (Manhattan) między odłamkiem <paramref name="p"/>, a punktem <paramref name="point"/>
        /// </summary>
        /// <param name="p"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private int Manhattan(Piece p, (int x, int y) point)
        {
            if (p.carried) return int.MaxValue;
            return Math.Abs(p.position.x - point.x) + Math.Abs(p.position.y - point.y);
        }

        /// <summary>
        /// Sprawdzenie możliwości wykonania ruchu w kierunku <paramref name="d"/> przez agenta o identyfikatorze <paramref name="id"/>
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        /// <param name="d"> Żądany kierunek ruchu </param>
        ///<returns> 
        ///0 - ruch się wykonał, 
        ///4 - gdy agent żądający ruchu jest w trakcie kary czasowej za poprzednią akcję, 
        ///5 - gdy agent próbuje wejść na pole, na którym znajduje się inny gracz, 
        ///7 - gdy ruch jest nieprawidłowy </returns>
        ///<exception cref="JsonException"></exception>
        private int Move(int id, Direction d)
        {
            if (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp)
            {
                return 4;
            }
            PlayerInfo p = players[id];
            switch (d)
            {
                case Direction.Up:
                    if ((p.team == 0 && p.position.y + 1 == map.GetLength(1) - goalAreaHeight) || p.position.y == map.GetLength(1) - 1)
                    {
                        return 7;
                    }
                    if (map[p.position.x, p.position.y + 1].player != null)
                    {
                        return 5;
                    }
                    map[p.position.x, p.position.y].player = null;
                    map[p.position.x, p.position.y + 1].player = p;
                    p.position.y++;
                    return 0;
                case Direction.Right:
                    if (p.position.x == map.GetLength(0) - 1)
                    {
                        return 7;
                    }
                    if (map[p.position.x + 1, p.position.y].player != null)
                    {
                        return 5;
                    }
                    map[p.position.x, p.position.y].player = null;
                    map[p.position.x + 1, p.position.y].player = p;
                    p.position.x++;
                    return 0;
                case Direction.Down:
                    if ((p.team == 1 && p.position.y == goalAreaHeight) || p.position.y == 0)
                    {
                        return 7;
                    }
                    if (map[p.position.x, p.position.y - 1].player != null)
                    {
                        return 5;
                    }
                    map[p.position.x, p.position.y].player = null;
                    map[p.position.x, p.position.y - 1].player = p;
                    p.position.y--;
                    return 0;
                case Direction.Left:
                    if (p.position.x == 0)
                    {
                        return 7;
                    }
                    if (map[p.position.x - 1, p.position.y].player != null)
                    {
                        return 5;
                    }
                    map[p.position.x, p.position.y].player = null;
                    map[p.position.x - 1, p.position.y].player = p;
                    p.position.x--;
                    return 0;
            }
            throw new JsonException("Unexpected direction");
        }


        /// <summary>
        /// Sprawdzenie możliwości podniesienia kawałka przez agenta o identyfikatorze <paramref name="id"/>
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        /// <returns> 
        /// 0 - gdy podniesienie jest możliwe, 
        /// 4 - gdy gracz jest w trakcie kary czasowej za poprzednią akcję, 
        /// 7 - gdy ruch jest nieprawidłowy </returns>
        private int PickPiece(int id)
        {
            if (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp)
            {
                return 4;
            }
            PlayerInfo p = players[id];
            if (p.carriedPiece != null || map[p.position.x, p.position.y].piece == null)
            {
                return 7;
            }
            p.carriedPiece = map[p.position.x, p.position.y].piece;
            p.carriedPiece.carried = true;
            map[p.position.x, p.position.y].piece = null;
            currentBoardPieces--;
            return 0;
        }

        /// <summary>
        /// Sprawdzenie czy agent o identyfikatorze <paramref name="id"/> może odłożyć kawałek
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        /// <returns> 
        /// 0 - gdy może i odkłada w strefie zadań (task area), 
        /// 1 - gdy może i odkłada na niespełnionym celu w strefie celów, 
        /// 2 - gdy może i odkłada na celu, który już został osiągnięty lub na polu niebędącym celem w strefie celów, 
        /// 3 - gdy może i odkłada w strefie celów, ale kawałek był fałszywy, 
        /// 4 - gdy nie może odłożyć, bo jest w trakcie kary czasowej po poprzedniej akcji
        /// 7 - gdy nie może, bo akcja jest nieprawidłowa (np. agent nie posiada odłamka, który mógłby sprawdzić) 
        /// </returns>
        private int PutPiece(int id)
        {
            if (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp)
            {
                return 4;
            }
            PlayerInfo p = players[id];
            if (p.carriedPiece == null || map[p.position.x, p.position.y].piece != null)
            {
                return 7;
            }
            if (p.position.y >= goalAreaHeight && p.position.y < map.GetLength(1) - goalAreaHeight)
            {
                map[p.position.x, p.position.y].piece = p.carriedPiece;
                p.carriedPiece.carried = false;
                p.carriedPiece.position = p.position;
                p.carriedPiece = null;
                currentBoardPieces++;
                return 0;
            }
            if (p.carriedPiece.real)
            {
                pieces.Remove(p.carriedPiece);
                p.carriedPiece = null;
                if (map[p.position.x, p.position.y].isGoal && !map[p.position.x, p.position.y].isAchieved)
                {
                    if (p.team == 0)
                    {
                        goals0--;
                        if (goals0 == 0)
                        {
                            EndGame(0);
                        }
                    }
                    else
                    {
                        goals1--;
                        if (goals1 == 0)
                        {
                            EndGame(1);
                        }
                    }
                    map[p.position.x, p.position.y].isAchieved = true;
                    return 1;
                }
                else
                {
                    map[p.position.x, p.position.y].isAchieved = true; // shows up as N on the map (achieved but not a goal)
                    return 2;
                }
            }
            pieces.Remove(p.carriedPiece);
            p.carriedPiece = null;
            return 3;
        }

        /// <summary>
        /// Sprawdzenie czy agent o identyfikatorze <paramref name="id"/> może sprawdzić odłamek
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        ///<returns>
        ///0 - gdy może sprawdzić odłamek i jest on prawdziwy
        ///1 - gdy może sprawdzić odłamek, ale jest on fałszywy
        ///4 - gdy nie może sprawdzić, ponieważ jest w trakcie kary czasowej za wcześniejszą akcję
        ///7 - gdy akcja jest nieprawidłowa (np. agent nie posiada odłamka do sprawdzenia) 
        ///</returns>
        private int CheckPiece(int id)
        {
            if (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp)
            {
                return 4;
            }
            PlayerInfo p = players[id];
            if (p.carriedPiece == null)
            {
                return 7;
            }
            return p.carriedPiece.real ? 0 : 1;
        }

        /// <summary>
        /// Sprawdzenie, czy agent o identyfikatorze <paramref name="id"/> może zniszczyć odłamek
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        ///<returns>
        ///0 - gdy może i jest to realizowane
        ///4 - gdy nie może, ponieważ jest w trakcie kary czasowej za poprzednią akcję
        ///7 - gdy akcja jest nieprawidłowa (np. agent nie posiada odłamka, który mógłby zniszczyć
        ///</returns>
        private int DestroyPiece(int id)
        {
            if (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp)
            {
                return 4;
            }
            PlayerInfo p = players[id];
            if (p.carriedPiece == null)
            {
                return 7;
            }
            pieces.Remove(p.carriedPiece);
            p.carriedPiece = null;
            return 0;
        }

        /// <summary>
        /// Sprawdzenie, czy agent o identyfikatorze <paramref name="id"/> może wykonać skan na obszarze 3x3
        /// </summary>
        /// <param name="id"> Identyfikator agenta </param>
        ///<returns>
        ///0 - gdy może i jest to realizowane
        ///4 - gdy nie może, ponieważ jest w trakcie kary czasowej za poprzednią akcję
        ///</returns>
        private int DiscoverField(int id)
        {
            return (_nextMoveTime.ContainsKey(id) && _nextMoveTime[id] > Timestamp) ? 4 : 0;
        }
        /// <summary>
        /// Utworzenie i umieszczenie nowego kawałka na mapie gry przez Mistrza Gry (GM)
        /// </summary>
        private void SpawnPiece()
        {
            int width = map.GetLength(0);
            int length = width * (map.GetLength(1) - goalAreaHeight * 2);
            if (currentBoardPieces < maxPieces && (DateTime.Now - timeOfLastPieceSpawn).TotalMilliseconds > pieceSpawnTime)
            {
                timeOfLastPieceSpawn = DateTime.Now;

                int offset = r.Next(length - currentBoardPieces);

                int count = 0, pos = 0;
                while (count < offset)
                {
                    if (map[pos % width, pos / width + goalAreaHeight].piece == null)
                        count++;
                    pos++;
                }
                while (map[pos % width, pos / width + goalAreaHeight].piece != null)
                    pos++;

                int x = pos % width;
                int y = pos / width + goalAreaHeight;

                map[x, y].piece = new Piece(r.NextDouble() > badPieceChance, (x, y));
                pieces.Add(map[x, y].piece);
                currentBoardPieces++;
            }
        }
        /// <summary>
        /// Wysyłanie zakodowanej odpowiedzi <paramref name="json"/> typu <paramref name="msgId"/> przez połączenie TCP
        /// </summary>
        /// <param name="json"></param>
        /// <param name="msgId"></param>
        private void SendResponse(string json, MessageType msgId)
        {
            connection.SendMessage(json, msgId);
        }
        
        /// <summary>
        /// Tworzenie połączenia TCP między serwerem komunikacyjnym o adresie <paramref name="address"/> na porcie <paramref name="port"/>
        /// </summary>
        bool Connect(string address, int port)
        {
            try
            {
                connection = new TcpClient(address, port);
            }
            catch (SocketException se)
            {
                string msg = "Something went wrong\nErrorCode: " + se.ErrorCode + "\n" + se.Message;
                switch (se.SocketErrorCode)
                {
                    case SocketError.HostNotFound:
                        msg = "HostNotFound";
                        break;
                    case SocketError.TimedOut:
                        msg = "The connection attempt timed out, or the connected host has failed to respond.";
                        break;
                    case SocketError.ConnectionRefused:
                        msg = "The remote host is actively refusing the connection.";
                        break;

                }
                MessageBox.Show(msg, "SocketException", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show(ae.Message, "Invalid argument", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Znajdowanie lidera drużyny, jeśli nie istnieje rzucany jest wyjątek
        /// </summary>
        /// <param name="team"> 0 or 1 </param>
        /// <returns> Identyfikator agenta, który jest liderem </returns>
        /// <exception cref="ApplicationException"/>
        int FindLeader(int team)
        {
            foreach (var player in players)
            {
                if (players[player.Key].team == team && players[player.Key].isLeader == true)
                    return player.Key;
            }
            throw new ApplicationException("No leader found");
        }
        /// <summary>
        /// Losowe rozmieszczanie graczy w strefie celów (goal area) na początku rozgrywki
        /// </summary>
        /// <param name="players"></param>
        void SetPlayersOnMap(Dictionary<int, PlayerInfo> players)
        {
            int width = map.GetLength(0);
            int length = width * goalAreaHeight;
            int tZeroMembers = 0;
            int tOneMembers = 0;

            int[] locations = new int[length];

            for (int i = 0; i < length; ++i)
                locations[i] = i;
            for (int i = 0; i < length; ++i)
            {
                int rand = i + r.Next(length - i);
                int tmp = locations[rand];
                locations[rand] = locations[i];
                locations[i] = tmp;
            }
            foreach (var player in players)
            {
                switch (players[player.Key].team)
                {
                    case 0:
                        {
                            players[player.Key].position.x = locations[tZeroMembers] % width;
                            players[player.Key].position.y = locations[tZeroMembers++] / width;
                            map[players[player.Key].position.x, players[player.Key].position.y].player = players[player.Key];
                            break;
                        }
                    case 1:
                        {
                            players[player.Key].position.x = locations[tOneMembers] % width;
                            players[player.Key].position.y = map.GetLength(1) - 1 - locations[tOneMembers++] / width;
                            map[players[player.Key].position.x, players[player.Key].position.y].player = players[player.Key];
                            break;
                        }
                }
            }

            for (int i = 0; i < length; ++i)
            {
                int rand = i + r.Next(length - i);
                int tmp = locations[rand];
                locations[rand] = locations[i];
                locations[i] = tmp;
            }
            for (int i = 0; i < goals0; i++)
            {
                map[locations[i] % width, locations[i] / width].isGoal = true;
                map[locations[i] % width, map.GetLength(1) - locations[i] / width - 1].isGoal = true;
            }
            
            length = width * (map.GetLength(1) - goalAreaHeight * 2);
            locations = new int[length];
            for (int i = 0; i < length; ++i)
                locations[i] = i;
            for (int i = 0; i < length; ++i)
            {
                int rand = i + r.Next(length - i);
                int tmp = locations[rand];
                locations[rand] = locations[i];
                locations[i] = tmp;
            }
            for (int i = 0; i < maxPieces; i++)
            {
                int x = locations[i] % width;
                int y = locations[i] / width + goalAreaHeight;

                map[x, y].piece = new Piece(r.NextDouble() > badPieceChance, (x, y));
                pieces.Add(map[x, y].piece);
            }

            timeOfLastPieceSpawn = DateTime.Now;
            currentBoardPieces = pieces.Count;
        }

        /// <summary>
        /// Rozpoczęcie gry w zadanymi w konstruktorze Mistrza Gry (GM) parametrami - rozesłanie wiadomości startowej do graczy
        /// </summary>
        public void StartGame()
        {
            SetPlayersOnMap(players);
            
            List<int> agentIdsTeam0 = new List<int>(numberOfPlayers / 2);
            List<int> agentIdsTeam1 = new List<int>(numberOfPlayers / 2);
            foreach (var player in players)
                if (player.Value.team == 0)
                    agentIdsTeam0.Add(player.Key);
                else
                    agentIdsTeam1.Add(player.Key);
            int[][] agentTeamIds = new int[2][];
            agentTeamIds[0] = agentIdsTeam0.ToArray();
            agentTeamIds[1] = agentIdsTeam1.ToArray();

            foreach (var player in players)
            {
                if (!_testMode)
                {
                    SendResponse(
                        "{\n" +
                        "\"msgId\": 32,\n" +
                        "\"agentId\": " + player.Key + ",\n" +
                        "\"agentIdsFromTeam\": " + JsonConvert.SerializeObject(agentTeamIds[player.Value.team]) +
                        ",\n" +
                        "\"teamLeaderId\": " + FindLeader(players[player.Key].team) + ",\n" +
                        "\"timestamp\": " + Timestamp + ",\n" +
                        "\"boardSizeX\": " + map.GetLength(0) + ",\n" +
                        "\"boardSizeY\": " + (map.GetLength(1) - goalAreaHeight * 2) + ",\n" +
                        "\"goalAreaHeight\": " + goalAreaHeight + ",\n" +
                        "\"initialXPosition\": " + players[player.Key].position.x + ",\n" +
                        "\"initialYPosition\": " + players[player.Key].position.y + ",\n" +
                        "\"numberOfGoals\": " + goals0 + ",\n" +
                        "\"numberOfPlayers\": " + numberOfPlayers + ",\n" +
                        "\"pieceSpawnDelay\": " + pieceSpawnTime + ",\n" +
                        "\"maxNumberOfPiecesOnBoard\": " + maxPieces + ",\n" +
                        "\"probabilityOfBadPiece\": " + badPieceChance.ToString().Replace(',', '.') + ",\n" +
                        "\"baseTimePenalty\": " + _baseTimePenalty + ",\n" +
                        "\"tpm_move\": " + _timePenaltyMultipliers[MessageType.MakeMoveRequest] + ",\n" +
                        "\"tpm_discoverPieces\": " + _timePenaltyMultipliers[MessageType.Discover3x3Request] + ",\n" +
                        "\"tpm_pickPiece\": " + _timePenaltyMultipliers[MessageType.PickPieceRequest] + ",\n" +
                        "\"tpm_checkPiece\": " + _timePenaltyMultipliers[MessageType.CheckPieceRequest] + ",\n" +
                        "\"tpm_destroyPiece\": " + _timePenaltyMultipliers[MessageType.DestroyPieceRequest] + ",\n" +
                        "\"tpm_putPiece\": " + _timePenaltyMultipliers[MessageType.PutPieceRequest] + ",\n" +
                        "\"tpm_infoExchange\": " + _timePenaltyMultipliers[MessageType.ExchangeInformationRequest] +
                        ",\n" +
                        "}", MessageType.GameStart);
                }
            }
            playing = true;
        }

        /// <summary>
        /// Wyświetlanie bieżącego stanu gry
        /// </summary>
        void DisplayMap()
        {
            if (myGameView == null || refreshCompare == myGameView.refreshCounter)
                return;

            refreshCompare = myGameView.refreshCounter;
            myGameView.UpdateMapView(map);
        }

        /// <summary>
        /// Zakończenie rozgrywki - wysłanie agentom wiadomości końcowej "Game Over" i wyświetlenie nazwy drużyny, która wygrała
        /// </summary>
        void EndGame(int winners)
        {
            foreach (var player in players)
            {
                connection.SendMessage(
                "{\n" +
                       "\"msgId\": 33,\n" +
                       "\"agentId\": " + player.Key + ",\n" +
                       "\"timestamp\": " + Timestamp + ",\n" +
                       "\"winningTeam\": " + winners + "\n" +
                   "}",
                MessageType.GameOver
                );
            }
            CloseServerConnection();
            DisplayStats(winners);
        }

        /// <summary>
        /// Obliczanie momentu, w którym agent o identyfikatorze <paramref name="playerId"/> będzie mógł wykonać kolejny ruch po akcji typu <paramref name="requestType"/>
        /// </summary>
        /// <param name="requestType"> Typ podejmowanej przez agenta akcji </param>
        /// <param name="playerId"> Identyfikator agenta</param>
        /// <returns></returns>
        private long WaitUntilTime(MessageType requestType, int playerId)
        {
            if (!_timePenaltyMultipliers.ContainsKey(requestType))
            {
                return _nextMoveTime.ContainsKey(playerId) ? _nextMoveTime[playerId] : Timestamp;
            }
            var time = Timestamp + _baseTimePenalty * _timePenaltyMultipliers[requestType];
            _nextMoveTime[playerId] = time;
            return time;
        }

        /// <summary>
        /// Wysłanie zakodowanej odpowiedzi <paramref name="message"/> typu <paramref name="msgId"/> i oczekiwanie na kolejną wiadomość
        /// </summary>
        /// <param name="message"></param>
        /// <param name="msgId"></param>
        /// <returns></returns>
        private string SendMessageAndWaitForResponse(string message, MessageType msgId)
        {
            connection.SendMessage(message, msgId);
            return connection.GetSingleMessage();
        }

        /// <summary>
        /// Wyświetlenie zwycięzców po zakończeniu gry
        /// </summary>
        /// <param name="winners"> Drużyna agentów, która zrealizowała wszystkie cele </param>
        void DisplayStats(int winners)
        {
            myGameView?.WinEnd(winners);
        }

        /// <summary>
        /// Zamykanie połączenia z serwerem komunikacyjnym
        /// </summary>
        void CloseServerConnection()
        {
            connection = null;
        }

        /// <summary>
        /// Informacje określające stan chwilowy agenta
        /// </summary>
        public class PlayerInfo
        {
            public int id;
            public int team; //0 or 1
            public bool isLeader;
            public (int x, int y) position;
            public Piece carriedPiece;
        }

        /// <summary>
        /// Reprezentacja odłamka na mapie gry
        /// </summary>
        public class Piece
        {
            public bool real;
            public bool carried = false;
            public (int x, int y) position;  //used only if piece is not carried

            public Piece(bool real, (int x, int y) position)
            {
                this.real = real;
                this.position = position;
            }
        }

        /// <summary>
        /// Reprezentacja pola na mapie gry
        /// </summary>
        public class Field
        {
            public bool isGoal = false;
            public bool isAchieved = false;
            public Piece piece;
            public PlayerInfo player;
        }
    }
}
