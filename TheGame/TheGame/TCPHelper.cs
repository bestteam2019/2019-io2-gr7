﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    /// <summary>
    /// Zapewnia funkcje czytania z/pisania do gniazd TCP
    /// </summary>
    public static class TCPHelper
    {
        private const int BufferSize = 1024;
        /// <summary>
        /// Czytanie dokładnie jednej wiadomości z gniazda TCP określonego parametrem <paramref name="client"/>
        /// </summary>
        /// <param name="client"></param>
        /// <returns> Odczytana wiadomość </returns>
        public static string GetSingleMessage(this TcpClient client)
        {
            var buffer = new byte[BufferSize];
            var stream = client.GetStream();
            stream.Read(buffer, 0, 8);
            var msgLength = BitConverter.ToInt32(buffer, 0);
            var msgId = BitConverter.ToInt32(buffer, 4);

            if (msgLength > buffer.Length)
            {
                buffer = new byte[msgLength];
            }
            
            var readBytesTotal = 0;
            var message = new StringBuilder(msgLength);
            do
            {
                var readBytes = stream.Read(buffer, 0, msgLength - readBytesTotal);
                message.Append(Encoding.UTF8.GetString(buffer, 0, readBytes));
                readBytesTotal += readBytes;
            } while (readBytesTotal < msgLength);

            //debugging
            MessageModel mm;
            try
            {
                mm = MessageModel.Decode(message.ToString());
                Console.WriteLine($"received messsage {msgId} from {(mm as MessageModelWithAgentId)?.agentId}");
            }
            catch (Exception e)
            {
                var z = e;
            }

            return message.ToString();
        }

        /// <summary>
        /// Pisanie wiadomości <paramref name="message"/> typu <paramref name="msgId"/> do odbiorcy określonego przez <paramref name="client"/>
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        /// <param name="msgId"></param>
        /// <param name="logForm"></param>
        public static void SendMessage(this TcpClient client, string message, MessageType msgId, LogForm logForm = null)
        {
            if (!client.Connected) return;
            var bytes = Encoding.UTF8.GetBytes(message);
            var msgLength = BitConverter.GetBytes(bytes.Length);
            var msgIdBytes = BitConverter.GetBytes(Convert.ToInt32(msgId));
            var msg = msgLength.Concat(msgIdBytes).ToArray();
            msg = msg.Concat(bytes).ToArray();
            var stream = client.GetStream();
            stream.Write(msg, 0, msg.Length);

            MessageModel mm;
            try
            {
                mm = MessageModel.Decode(message);
                Console.WriteLine($"sent messsage {mm.msgId} to {(mm as MessageModelWithAgentId)?.agentId}");
            }
            catch (Exception e)
            {
                var z = e;
            }

        }
    }
}
