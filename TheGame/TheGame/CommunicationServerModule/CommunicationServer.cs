﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TheGame.Abstract;

namespace TheGame.CommunicationServerModule
{
    /// <summary>
    /// Reprezentacja serwera utrzymującego połączenie TCP między agentami i Mistrzem Gry (GM)
    /// </summary>
    public class CommunicationServer : ICommunicationServer
    {
        private GameState _state = GameState.Initializing;
        private int _listenerPort;
        private TcpListener _TCPListener;
        private Task _listeningTask;

        private readonly string _token = "TOKEN";

        private readonly Dictionary<TcpClient, int> _agentsConnections = new Dictionary<TcpClient, int>();
        private TcpClient _gmConnection;
        private readonly HashSet<TcpClient> _connections = new HashSet<TcpClient>();

        private readonly List<Task> _tcpClientTasks = new List<Task>();

        private static int _ids = 1;
        private bool logWhenGameEnds;
        private LogForm _myLogForm;

        /// <summary>
        /// Utworzenie okna, w którym logowane są informacje przychodzące od agentów
        /// </summary>
        /// <param name="form"></param>
        public void AddLogForm(LogForm form)
        {
            _myLogForm = form;
            _myLogForm.Show();
            _myLogForm.Log("I did a thing!");
            _myLogForm.Log($"Server on {Dns.GetHostName()}");
        }
        /// <summary>
        /// Rozpoczęcie oczekiwania na połączenie agentów
        /// </summary>
        public void StartListening()
        {
            if (_state == GameState.Closed)
            {
                throw new Exception("Server dead, game ended");
            }

            if (_listeningTask != null)
            {
                throw new Exception("Server is already listening");
            }

            if (_listenerPort < 1) 
            {
                throw new Exception("Cannot start listening, server was not configured");
            }

            _TCPListener = new TcpListener(IPAddress.Any, _listenerPort);

            _listeningTask = new Task(AcceptAgentsConnections);
            _listeningTask.Start();
        }
        /// <summary>
        /// Dodanie agentów próbujących się połączyć i oczekiwanie na żądania od nich
        /// </summary>
        private void AcceptAgentsConnections()
        {
            _TCPListener.Start();
            while (true)
            {
                var client = _TCPListener.AcceptTcpClient();
                _connections.Add(client);

                var clientListener = new Task(() => ListenToClient(client));
                _tcpClientTasks.Add(clientListener);
                clientListener.Start();
            }
        }
        /// <summary>
        /// Odbieranie wiadomości od Mistrza Gry (GM) lub agentów 
        /// </summary>
        /// <param name="client"></param>
        private void ListenToClient(TcpClient client)
        {
            while (true)
            {
                var msg = client.GetSingleMessage();
                if(!ReceiveRequest(msg, client)) break;
            }
        }
        /// <summary>
        /// Wysyłanie wiadomości <paramref name="message"/> typu <paramref name="msgId"/> do odbiorcy (<paramref name="recipient"/>)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="recipient"></param>
        /// <param name="msgId"></param>
        private void SendMessage(string message, TcpClient recipient, MessageType msgId)
        {
            recipient.SendMessage(message, msgId);
        }
        /// <summary>
        /// Przetwarzanie w pętli przychodzących od nadawcy (<paramref name="sender"/>)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        public bool ReceiveRequest(string message, TcpClient sender)
        {
            bool continueLoop = true;
            lock (_token)
            {
                try
                {
                    if (!_connections.Contains(sender))
                    {
                        throw new Exception("Only connections added via AddConnection can send requests");
                    }
                    switch (_state)
                    {
                        case GameState.Initializing:
                            ProcessRequestInitializing(message, sender);
                            break;
                        case GameState.Running:
                            ProcessRequestRunning(message, sender);
                            break;
                        case GameState.Closing:
                            ProcessRequestClosing(message, sender);
                            break;
                        case GameState.Closed: throw new Exception("Server dead, game ended");
                    }
                }
                catch (Exception e)
                {
                    var ee = e;
                    continueLoop = false;
                }
            }
            return continueLoop;
        }
        /// <summary>
        /// Przetwarzanie wiadomości <paramref name="message"/> od nadawcy (<paramref name="sender"/>) podczas rozgrywki
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        private void ProcessRequestRunning(string message, TcpClient sender)
        {
            var decodedMessage = DecodeMessage(message, out _);
            if (sender == _gmConnection)
            {
                ProcessGMRequest(decodedMessage);
            }
            else if (_agentsConnections.ContainsKey(sender))
            {
                ProcessPlayerRequest(sender, decodedMessage);
            }
            else
            {
                SendMessage(new InvalidActionMessage().ToJson(), sender, MessageType.InvalidAction);
            }
        }
        /// <summary>
        /// Przetwarzanie zdekodowanej wiadomości (<paramref name="decodedMessage"/>) od nadawcy (<paramref name="sender"/>)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="decodedMessage"></param>
        private void ProcessPlayerRequest(TcpClient sender, MessageModel decodedMessage)
        {
            //if ((int)decodedMessage.Type() < 64 || (int)decodedMessage.Type() > 69)
            //{
            //    SendMessage(new InvalidActionMessage().ToJson(), sender, MessageType.InvalidAction);
            //}

            var playerRequest = (MessageModelWithAgentId)decodedMessage;
            playerRequest.agentId = _agentsConnections[sender];
            SendMessage(playerRequest.ToJson(), _gmConnection, playerRequest.msgId);
        }

        /// <summary>
        /// Przetwarzanie zdekodowanej wiadomości (<paramref name="decodedMessage"/>) od Mistrza Gry (GM)
        /// </summary>
        /// <param name="decodedMessage"></param>
        private void ProcessGMRequest(MessageModel decodedMessage)
        {
            if (decodedMessage.Type() == MessageType.GameOver)
            {
                _state = GameState.Closing;
                foreach (var agent in _agentsConnections.Select(x => x.Key))
                {
                    SendMessage(decodedMessage.ToJson(), agent, decodedMessage.msgId);
                }
                Shutdown();
                return;
            }

            var recipient = _agentsConnections.SingleOrDefault(c => c.Value == ((MessageModelWithAgentId)decodedMessage).agentId).Key;
            if (recipient == null)
            {
                SendMessage(new InvalidActionMessage().ToJson(), _gmConnection, MessageType.InvalidAction);
            }
            else
            {
                SendMessage(decodedMessage.ToJson(), recipient, decodedMessage.msgId);
                
            }
        }

        /// <summary>
        /// Zakończenie pracy serwera
        /// </summary>
        private void Shutdown()
        {
            var task = new Task(() =>
            {
                System.Threading.Thread.Sleep(3000);
                if (logWhenGameEnds)
                    _myLogForm.WriteTextToFile("server_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".txt");
                _state = GameState.Closed;
            });
            task.Start();
        }
        /// <summary>
        /// Przetwarzanie otrzymywanych wiadomości w trybie kończenia rozgrywki
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        private void ProcessRequestClosing(string message, TcpClient sender)
        {
            return;
        }
        /// <summary>
        /// Przetwarzanie otrzymywanych wiadomości w trybie rozpoczynania rozgrywki - akceptowanie połączeń agentów i Mistrza Gry
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        private void ProcessRequestInitializing(string message, TcpClient sender)
        {
            MessageType msgId = MessageType.InvalidAction;
            var decodedMessage = DecodeMessage(message, out _);
            var response = "";
            switch (decodedMessage.Type())
            {
                case MessageType.GMJoinToGameRequest:
                    if (_gmConnection != null)
                    {
                        response = new GMJoinToGameResponseMessage { isConnected = false }.ToJson();
                        msgId = MessageType.GMJoinToGameResponse;
                        break;
                    }
                    _gmConnection = sender;
                    response = new GMJoinToGameResponseMessage { isConnected = true }.ToJson();
                    msgId = MessageType.GMJoinToGameResponse;
                    break;
                case MessageType.JoinToGameRequest when sender != _gmConnection:
                    if (_gmConnection == null)
                    {
                        response = new GMNotConnectedYetMessage().ToJson();
                        msgId = MessageType.GMNotConnectedYet;
                    }
                    else
                    {
                        var assignedId = _ids++;
                        _agentsConnections.Add(sender, assignedId);
                        var joinGameRequest = (JoinToGameRequestMessage)decodedMessage;
                        joinGameRequest.agentId = assignedId;
                        SendMessage(joinGameRequest.ToJson(), _gmConnection, joinGameRequest.msgId);
                        return;
                    }
                    break;
                case MessageType.JoinToGameResponse when sender == _gmConnection:
                    var responseDecoded = decodedMessage as JoinToGameResponseMessage;
                    if (responseDecoded == null) return;
                    var agentConnection = _agentsConnections.FirstOrDefault(x => x.Value == responseDecoded.agentId).Key;
                    if (responseDecoded.isConnected == false)
                    {
                        _agentsConnections.Remove(agentConnection);
                    }
                    SendMessage(responseDecoded.ToJson(), agentConnection, responseDecoded.msgId);
                    return;
                case MessageType.GameStart when sender == _gmConnection:
                    _state = GameState.Running;
                    ProcessRequestRunning(message, sender);
                    return;
                //dispose pending_connections
                //foreach (var agent in _agentsConnections.Select(x => x.Key))
                //{
                //    agent.SendRequestToParticipant(message);
                //}
                //return null;
                default:
                    response = new InvalidActionMessage().ToJson();
                    msgId = MessageType.InvalidAction;
                    break;
            }
            SendMessage(response, sender, msgId);
        }

        /// <summary>
        /// Dekodowanie wiadomości z postaci json'a <paramref name="message"/> i zapisanie jej typu w <paramref name="type"/>
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <returns> Obiekt reprezentujący zdekodowaną wiadomość </returns>
        private MessageModel DecodeMessage(string message, out MessageType type)
        {
            try
            {
                return MessageModel.Decode(message, out type);
            }
            catch (Exception e)
            {
                throw e;
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Konfiguracja portu serwera komunikacyjnego
        /// </summary>
        /// <param name="port"></param>
        public void Configure(int port)
        {
            _listenerPort = port;
        }
        /// <summary>
        /// Włączenie lub wyłączenie trybu logowania
        /// </summary>
        /// <param name="logOption"></param>
        public void Logging(bool logOption)
        {
            logWhenGameEnds = logOption;
        }
    }
}
