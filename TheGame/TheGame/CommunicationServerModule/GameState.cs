﻿namespace TheGame.CommunicationServerModule
{
    public enum GameState
    {
        Initializing,
        Running,
        Closing,
        Closed
    }
}