﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TheGame
{
    /// <summary>
    /// Okno pozwalające na logowanie wykonywanych akcji
    /// </summary>
    public partial class LogForm : Form
    {
        private string fullLog;
        private int maxDispLength;
        private int batchSize;
        private int batchIndex;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="whoseLogIsItAnyway">Tytuł okna informujący do kogo ono należy (np. gracz z drużyny 1 o id 4)</param>
        /// <param name="maxDispLength">Maksymalna długość wyświetlanej porcji loga</param>
        /// <param name="batchSize">Ilość wpisów do loga jaką okno grupuje w jedną aktualizację wyświetlanej porcji loga</param>
        public LogForm(string whoseLogIsItAnyway, int maxDispLength = 4096, int batchSize = 1)
        {
            InitializeComponent();
            this.Text = whoseLogIsItAnyway;
            this.maxDispLength = maxDispLength;
            this.batchSize = batchSize;
            batchIndex = batchSize - 1;
        }

        /// <summary>
        /// Dodaje nową linijkę do loga wraz z timestampem,
        /// aktualizuje wyświetlaną część loga jeśli pozwalają na to parametry okna
        /// </summary>
        /// <param name="text">Tekst który zostanie dopisany do loga</param>
        public void Log(string text)
        {
            Invoke((MethodInvoker)delegate
            {
                fullLog += DateTime.Now.ToString("HH:mm:ss.fff") + " > " + text + Environment.NewLine;

                batchIndex++;
                if (maxDispLength < 1 || batchIndex < batchSize)
                    return;

                int dispIndex = fullLog.Length - maxDispLength;
                if (dispIndex < 0)
                    dispIndex = 0;
                else
                    dispIndex = fullLog.IndexOf('\n', dispIndex);

                logTextBox.Text = fullLog.Substring(dispIndex);
                logTextBox.SelectionLength = 0;
                logTextBox.SelectionStart = logTextBox.Text.Length;
                logTextBox.ScrollToCaret();

                batchIndex = 0;
            });
        }

        /// <summary>
        /// Wyświetla całość loga (np. po zakończeniu gry)
        /// </summary>
        public void DisplayFull()
        {
            Invoke((MethodInvoker)delegate
            {
                logTextBox.Text = fullLog;
                logTextBox.SelectionLength = 0;
                logTextBox.SelectionStart = logTextBox.Text.Length;
                logTextBox.ScrollToCaret();
            });
        }

        /// <summary>
        /// Zapisuje loga do pliku w folderze obok pliku wykonywalnego gry
        /// </summary>
        /// <param name="fileName">Nazwa pod jaką ma zostać zapisany log z tego okna</param>
        public void WriteTextToFile(string fileName)
        {
            Invoke((MethodInvoker)delegate
            {
                string dirPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\TheGameLogs";
                Directory.CreateDirectory(dirPath);
                Directory.SetCurrentDirectory(dirPath);
                File.WriteAllText(fileName, logTextBox.Text);
            });
        }
    }
}
