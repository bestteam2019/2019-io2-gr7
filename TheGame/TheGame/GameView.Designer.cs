﻿namespace TheGame
{
    partial class GameView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.teamZeroGroupBox = new System.Windows.Forms.GroupBox();
            this.centerGroupBox = new System.Windows.Forms.GroupBox();
            this.teamOneGroupBox = new System.Windows.Forms.GroupBox();
            this.mainTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.ColumnCount = 1;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTableLayoutPanel.Controls.Add(this.teamZeroGroupBox, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.centerGroupBox, 0, 1);
            this.mainTableLayoutPanel.Controls.Add(this.teamOneGroupBox, 0, 2);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 3;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(984, 562);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // teamZeroGroupBox
            // 
            this.teamZeroGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.teamZeroGroupBox.ForeColor = System.Drawing.Color.Red;
            this.teamZeroGroupBox.Location = new System.Drawing.Point(3, 3);
            this.teamZeroGroupBox.Name = "teamZeroGroupBox";
            this.teamZeroGroupBox.Size = new System.Drawing.Size(978, 204);
            this.teamZeroGroupBox.TabIndex = 0;
            this.teamZeroGroupBox.TabStop = false;
            this.teamZeroGroupBox.Text = "Team 1 goal area";
            // 
            // centerGroupBox
            // 
            this.centerGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerGroupBox.Location = new System.Drawing.Point(3, 213);
            this.centerGroupBox.Name = "centerGroupBox";
            this.centerGroupBox.Size = new System.Drawing.Size(978, 204);
            this.centerGroupBox.TabIndex = 1;
            this.centerGroupBox.TabStop = false;
            this.centerGroupBox.Text = "Center area";
            // 
            // teamOneGroupBox
            // 
            this.teamOneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.teamOneGroupBox.ForeColor = System.Drawing.Color.Blue;
            this.teamOneGroupBox.Location = new System.Drawing.Point(3, 423);
            this.teamOneGroupBox.Name = "teamOneGroupBox";
            this.teamOneGroupBox.Size = new System.Drawing.Size(978, 136);
            this.teamOneGroupBox.TabIndex = 2;
            this.teamOneGroupBox.TabStop = false;
            this.teamOneGroupBox.Text = "Team 0 goal area";
            // 
            // GameView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Name = "GameView";
            this.Text = "GameMaster";
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.GroupBox teamZeroGroupBox;
        private System.Windows.Forms.GroupBox centerGroupBox;
        private System.Windows.Forms.GroupBox teamOneGroupBox;
    }
}