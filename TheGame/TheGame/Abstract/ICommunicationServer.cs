﻿using System.Net.Sockets;

namespace TheGame.Abstract
{
    public interface ICommunicationServer
    {
        void AddLogForm(LogForm form);
        void StartListening();
        bool ReceiveRequest(string message, TcpClient sender);
        void Configure(int port);
        void Logging(bool logOption);
    }
}