﻿namespace TheGame
{
    //TODO: na pewno? Może to usunąć?

    /// <summary>
    /// Lista typów wiadomości, które są obsługiwane przez zaimplementowany deserializator JSON'ów
    /// </summary>
    public enum MessageType
    {
        GMNotConnectedYet = 6,
        InvalidAction = 7,
        GameStart = 32,
        GameOver = 33,
        JoinToGameRequest = 48,
        JoinToGameResponse = 49,
        GMJoinToGameRequest = 50,
        GMJoinToGameResponse = 51,
        MakeMoveRequest = 64,
        Discover3x3Request = 65,
        PickPieceRequest = 66,
        CheckPieceRequest = 67,
        DestroyPieceRequest = 68,
        PutPieceRequest = 69,
        MakeMoveResponse = 128,
        Discover3x3Response = 129,
        ExchangeInformationRequest = 71,
        ExchangeInformationWithDataRequest = 70,
        RequestDuringTimePenaltyResponse = 4,
        PickPieceResponse = 130,
        CheckPieceResponse = 131,
        PutPieceResponse = 133,
        DestroyPieceResponse = 132,
        CantMoveInThisDirection = 5,
        ExchangeInformationWithDataResponse = 134,
        ExchangeInformationWithDataAgreement = 72
    }
}