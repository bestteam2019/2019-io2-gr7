Metodologia:
- co tydzień spotykamy się (poza zajęciami) i ustalamy kto nad czym będzie pracował w danym tygodniu,
- wykonujemy rzeczy z backlogu, a jeśli znajdziemy coś, czego w nim nie ma - dodajemy nowe zadanie do backlogu,
- poza zajęciami (piątek 17:15-20:00) pracujemy w zależności od swoich możliwości - dobrze, żeby każdy spędzał w tygodniu co najmniej 4 godziny nad projektem (nie wliczając zajęć)
- ukończone zadania przenosimy do sekcji "Done :)" w tablicy zadań
- zadania niewykonane na czas przenosimy do sekcji "Should have been done :(" w tablicy zadań
- priorytetowo wykonujemy zadania z sekcji "Should have been done :("
- testy? HELP

Technologie:
- HELP

