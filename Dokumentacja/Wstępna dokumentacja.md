# Dokumentacja wstępna #

Opisana w tym dokumencie aplikacja ma służyć nauce metod projektowania
złożonych aplikacji, tworzenia specyfikacji i pracowania w zespole (diagramy
UML, metodyka waterfall).

W praktyce jest to symulacja (tzn. jedynym człowiekiem jest Użytkownik
uruchamiający symulację) prostej gry, w której dwie drużyny Graczy na jednej
planszy starają się jak najszybciej wykonać szereg zadań (umieścić pionki na
zadanych polach), zmagając się z ograniczoną wiedzą za pomocą komunikacji i
koordynacji.

Ponadto, każdy z uczestników gry (Mistrz Gry, Serwer Komunikacyjny, Gracze)
może działać na innym komputerze i komunikować się z pozostałymi przez sieć.

Dzięki temu będzie możliwe testowanie zgodności ze specyfikacją, poprzez
krzyżowanie modułów stworzonych przez różne zespoły (np. uruchomienie gry z
Mistrzem Gry zespołu A, Serwerem zespołu B oraz mieszanką Graczy w
implementacji zespołów A i B).

### Metodyka ###

* Waterfall - w poprzednim semestrze nasza grupa pracowała nad naszą wersją specyfikacji, w tym zajmujemy się tylko implementacją wybranej wersji
* Kanban - do organizacji pracy nad zadaniami, poza tym Trello jest dostępne przez Bitbucketa
	- co tydzień spotykamy się (poza zajęciami) i ustalamy kto nad czym będzie pracował w danym tygodniu,
	- wykonujemy rzeczy z backlogu, a jeśli znajdziemy coś, czego w nim nie ma - dodajemy nowe zadanie do backlogu,
	- poza zajęciami (piątek 17:15-20:00) pracujemy w zależności od swoich możliwości - dobrze, żeby każdy spędzał w tygodniu co najmniej 4 godziny nad projektem (nie wliczając zajęć)
	- ukończone zadania przenosimy do sekcji "Done :)" w tablicy zadań
	- zadania niewykonane na czas przenosimy do sekcji "Should have been done :(" w tablicy zadań
	- priorytetowo wykonujemy zadania z sekcji "Should have been done :("

### Technologie ###

* Może .NET Framework z Winformsami/WPFem
* Może ASP.NET Core WebApp, jeśli konieczne będzie korzystanie z Bitbucketowych Pipeline'ów
* Może .NET Core 3.0 Preview z Winformsami/WPFem (porty jeszcze nieukończone, ale wygląda na to, że praca wre)
* Zdecydowanie C#

### Ogólny podział zadań ###

* Mechanika gry
* Interfejs użytkownika (podgląd rozgrywki)
* Komunikacja przez sieć
* Strategie graczy